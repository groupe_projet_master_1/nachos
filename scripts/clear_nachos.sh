#!/bin/bash
cd ../code/build
if [ "$1" == "-m" ]
then
	echo "Make clean"
	make clean
	echo "Make"
	make
fi
echo "Nettoyage de l'environement nachOS"
./nachos-userprog -f &
PID=$!
sleep 1
kill -INT $PID
echo "Fin de nettoyage de l'environement nachOS"

#cd ../scripts