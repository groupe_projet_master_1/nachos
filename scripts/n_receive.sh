#!/bin/bash
cd ../code/build
if [ "$1" == "-rl" ]; then
	./nachos-userprog -m 1 -rl $2 -x ros
else
	./nachos-userprog -m 1 -x ros
fi