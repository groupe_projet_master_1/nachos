#!/bin/bash

# Affiche la liste du contenu de la machine NachOS

cd ../code/build
./nachos-userprog -l &
PID=$!
sleep 1
kill -INT $PID