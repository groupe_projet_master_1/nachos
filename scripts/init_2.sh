#!/bin/bash
./clear_nachos.sh

cd ../code/build
./nachos-userprog -cp ../test/network/test.txt test.txt &
PID=$!
sleep 1
kill -INT $PID
echo "Fichier test.txt copié"
cd ../../scripts

./copy.sh 3_receiveOverSize ros
./copy.sh 4_sendOverSize n_4
./copy.sh 5_getFile n_5


