#!/bin/bash
cd ../code/build
./nachos-userprog -p $1 &
PID=$!
sleep 1
kill -INT $PID