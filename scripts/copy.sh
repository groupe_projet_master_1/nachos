#!/bin/bash
chemin=""
cd ../code/build
for firstFile in *
do
	if [ "$1" == "$firstFile" ]; then
		chemin=$firstFile
		break
	else
		if [ -d $firstFile ]; then
			for secondFile in $firstFile/*
			do
				if [ "$firstFile/$1" == "$secondFile" ]; then
					chemin=$secondFile
					break
				fi
			done
		fi
	fi
done
if [ ! $chemin == "" ]; then
	./nachos-userprog -cp $chemin $2 &
	PID=$!
	sleep 1
	kill -INT $PID
	echo "Fichier $chemin copié sous $2"
fi
#cd ../scripts