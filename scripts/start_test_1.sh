#!/bin/bash
cd ../code/build
clear

echo "Fichier de test : 1_basic_thread"
echo ""
./nachos-userprog -x t_1 &
read -p ""
clear

echo "Fichier de test : 1_sem"
echo ""
./nachos-userprog -x s_1 &
read -p ""
clear

echo "Fichier de test : 2_multi_forkexec"
echo ""
./nachos-userprog -x p_2 &
read -p ""
clear

echo "Fichier de test : 3_join_proc"
echo ""
./nachos-userprog -x p_3 &
read -p ""
clear

#echo "Fichier de test : fichier"
#echo ""
# ./nachos-userprog -x f_1 &
# read -p ""
# clear