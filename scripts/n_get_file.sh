#!/bin/bash

# Affiche la liste du contenu de la machine NachOS

cd ../code/build
if [ "$1" == "-rl" ]; then
	./nachos-userprog -m 0 -rl $2 -x n_5
else
	./nachos-userprog -m 0 -x n_5
fi