#include "syscall.h"

int main() {
    int i;
    for(i = 0; i < 30; i++) {
        int pid = ForkExec("us0");
        ProcJoin(pid);
    }
    return 0;
}