#include "syscall.h"

typedef struct{
	sem_t prod;
	sem_t conso;
} semaphore;

char c;
semaphore sem;

//Affiche la chaine
void consommateur() {
	while(1){
		P(sem.conso);
		if(c == '\0'){
			return;
		}
		PutChar(c);
		V(sem.prod);
	}
}

void producteur(void *s){
	char *chaine = (char *)s;
	int i = 0;
	while(1){
		c = chaine[i];
		V(sem.conso);
		if(chaine[i] == '\0'){
			return;
		}
		P(sem.prod);
		i++;
	}
}

int main(){
	sem.prod = SemInit(0);
	sem.conso = SemInit(0);
	ThreadCreate(consommateur,0);
	ThreadCreate(producteur,(void *)"Bonjour je suis un producteur !\n");
	return 0;
}