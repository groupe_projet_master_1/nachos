#include "syscall.h"

int main()
{
    char *data = "é123456";
    PutString("Test message court avec accent, 7 caractères :\n");
    PutString("envoyé :");
    PutString(data);
    PutChar('\n');
    int n = Send(1, data);
    PutString("code de retour :");
    PutInt(n);
    PutChar('\n');
    PutChar('\n');

    PutString("Test message long, 400 caractères :\n");
    PutString("envoyé :");
    char *data3 = "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth ab";

    PutString(data3);
    n = Send(1, data3);
    PutChar('\n');
    PutString("code de retour :");
    PutInt(n);
    PutChar('\n');
    PutInt(n);
    return (0);
}
