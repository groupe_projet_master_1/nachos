#include "syscall.h"

int main() {
    PutChar('a');               //Ok / char             : affiche a
    PutChar(*"oui");            //Ok / char* -> char    : affiche o
    PutChar(64);                //Ok / int == char      : affiche @
    PutChar(63.99);             //Ok / float -> int     : affiche ?
    PutChar('\n');              //Pour faire propre
    return 0;
}
