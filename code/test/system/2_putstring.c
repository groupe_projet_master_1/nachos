#include "syscall.h"

//Si 'Si j'apparais entièrement, je fonctionne': ok
int main() {
    PutString("Bonjour, je suis plutôt long. Certainement plus long que la limite du système. Si j'apparais entièrement, je fonctionne. Je rajouterais même ÿ !");
    //PutString(64);                //Erreur de compilation : type incorrect
    //PutString((char*)64);         //Ne fonctionne pas ? → pas de \0 à étudier
    //PutString((char*)'a');        //Ne fonctionne pas ? → pas de \0 à étudier
    PutChar('\n');
    return 0;
}
