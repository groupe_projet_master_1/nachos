#include "syscall.h"

//Si '-2147483647' et '2147483647' : ok
int main() {
    int n = -2147483647;
    PutInt(n);                  //Affiche MIN_INT
    PutChar('\n');
    PutInt(n-1);                //Affiche un message d'erreur
    PutChar('\n');
    PutInt(10.5);               //Affiche 10: 10.5 est un float, pas un int
    PutChar('\n');
    return 0;
}