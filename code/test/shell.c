#include "syscall.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

#define ROOT "root"
#define PROMPT "nachos-shell$ "

#define CHANGE_DIRECTORY "cd"
#define CREATE_DIRECTORY "mkdir"
#define CREATE_FILE "touch"
#define EXIT "exit"
#define HELP "help"
#define LIST "ls"
#define NET_MESSAGE "mesg"
#define NET_RECEIVE "msgrcv"
#define NET_RECEIVE_FTP "ftprcv"
#define NET_SEND "msgsnd"
#define NET_SEND_FTP "ftpsnd"
#define NET_WRITE "write"
#define READ_FILE "rf"
#define REMOVE_DIRECTORY "rmdir"
#define REMOVE_FILE "rm"
#define WRITE_FILE "wf"

#define NET_BUFFER_SIZE 512

int isNetMessage = 0;
char netbuffer[NET_BUFFER_SIZE];

//----------------------------------------------------------------------
// StringSim
//	If s and z are similar, return 1. Otherwise, return 0.
//
//	s -- string to compare with z
//	z -- string to compare with s
//----------------------------------------------------------------------

int StringSim(char *s, char *z) {
	int i = 0;
	while (s[i] != '\0' && z[i] != '\0') {
		if (s[i] != z[i]) {
			return 0;
		}
		i++;
	}
	if (s[i] == '\0' && z[i] == '\0') {
		return 1;
	}
	return 0;
}

//----------------------------------------------------------------------
// StringSep
//----------------------------------------------------------------------

char *StringSep(char *from, char *to) {
	int i = 0;
	while (from[i] != '\0' && from[i] != ' ' && from[i] != '\n') {
		to[i] = from[i];
		i++;
	}
	to[i] = '\0';
	from = from + i;
	if (from[0] != '\0') {
		from = from + 1;
	}
	return from;
}

//----------------------------------------------------------------------
// StringToNum
//	Convert string to integer.
//	Only positive.
//----------------------------------------------------------------------

int StringToNum(char *from) {
	int i = 0;
	int n;
	int result = 0;
	while (from[i] != '\0') {
		n = from[i] - '0';
		if (n > 9) {
			return 0;
		}
		result = result * 10;
		result += n;
		i++;
	}
	return result;
}

//----------------------------------------------------------------------
// NetReceive
//----------------------------------------------------------------------

void NetReceive(void *netid) {
	int id = (int) netid;
	while (isNetMessage) {
		netbuffer[0] = '\0';
		Receive(id, netbuffer, NET_BUFFER_SIZE);
		PutString("\n");
		PutInt(id);
		PutString(" : ");
		PutString(netbuffer);
		PutString("\n");
	}
}

//----------------------------------------------------------------------
// main
//----------------------------------------------------------------------

int main () {
	int newProc;
	//OpenFileId input = ConsoleInput;
	//OpenFileId output = ConsoleOutput;
	char command[10], arg1[60], arg2[60];
	char buffer[NET_BUFFER_SIZE], *buff;
	int i, fid, result;
	int netThreadId = -1;

	PutString("Use \"help\" command to display list of commands\n");

	while (1) {
		PutString(KMAG);
		PutString (ROOT);
		PWD();
		PutString ("\n");
		PutString(KCYN);
		PutString (PROMPT);
		PutString(KNRM);

		i = 0;
		fid = -1;
		result = 0;
		buffer[0] = '\0';
		command[0] = '\0';
		arg1[0] = '\0';
		arg2[0] = '\0';

		do {
			GetString (&buffer[i], 1);
		} while (buffer[i++] != '\n');

		buffer[--i] = '\0';

		if (i > 0) {
			buff = StringSep(buffer, command);
			if (StringSim(command, CHANGE_DIRECTORY)) {
				StringSep(buff, arg1);
				result = ChangeDirectory(arg1);
				if (! result) {
					PutString("Could not change directory\n");
				}
			}
			else if (StringSim(command, CREATE_DIRECTORY)) {
				StringSep(buff, arg1);
				result = CreateDirectory(arg1);
				if (! result) {
					PutString("Could not create directory\n");
				}
			}
			else if (StringSim(command, CREATE_FILE)) {
				buff = StringSep(buff, arg1);
				StringSep(buff, arg2);
				result = Create(arg1, StringToNum(arg2));
				if (! result) {
					PutString("Could not create file\n");
				}
			}
			else if (StringSim(command, EXIT)) {
				break;
			}
			else if (StringSim(command, HELP)) {
				PutString(CHANGE_DIRECTORY);
				PutString("\t<path>");
				PutString("\t\t\tChange directory\n");
				PutString(CREATE_DIRECTORY);
				PutString("\t<name>");
				PutString("\t\t\tCreate a directory\n");
				PutString(CREATE_FILE);
				PutString("\t<filename> <size>");
				PutString("\tCreate a file\n");
				PutString(EXIT);
				PutString("\t\t\t\tClose shell\n");
				PutString(HELP);
				PutString("\t\t\t\tDisplay commands\n");
				PutString(LIST);
				PutString("\t\t\t\tList files in working directory\n");
				PutString(NET_MESSAGE);
				PutString("\t<netid> <y/n>");
				PutString("\t\tOpen a network messaging connection\n");
				PutString(NET_RECEIVE);
				PutString("\t<netid>");
				PutString("\t\t\tReceive one network sent message\n");
				PutString(NET_RECEIVE_FTP);
				PutString("\t<netid> <filename>");
				PutString("\tReceive one network sent file\n");
				PutString(NET_SEND);
				PutString("\t<netid> <message>");
				PutString("\tSend one message through network\n");
				PutString(NET_SEND_FTP);
				PutString("\t<netid> <filename>");
				PutString("\tSend one file through network\n");
				PutString(NET_WRITE);
				PutString("\t<netid> <message>");
				PutString("\tWrite through opened connection (use after mesg)\n");
				PutString(READ_FILE);
				PutString("\t<filename>");
				PutString("\t\tPrint out file content\n");
				PutString(REMOVE_DIRECTORY);
				PutString("\t<name>");
				PutString("\t\t\tRemove a directory\n");
				PutString(REMOVE_FILE);
				PutString("\t<filename>");
				PutString("\t\tRemove a file\n");
				PutString(WRITE_FILE);
				PutString("\t<filename> <text>");
				PutString("\tWrite to file\n");
			}
			else if (StringSim(command, LIST)) {
				List();
			}
			else if (StringSim(command, NET_MESSAGE)) {
				buff = StringSep(buff, arg1);
				StringSep(buff, arg2);
				if (arg2[0] == 'n') {
					isNetMessage = 0;
					PutString("Messaging disabled\n");
				}
				else if (arg2[0] == 'y') {
					isNetMessage = 1;
					netThreadId = ThreadCreate(&NetReceive, (void *) StringToNum(arg1));
					PutString("Messaging enabled\n");
				}
				else {
					PutString("Use \"");
					PutString(NET_MESSAGE);
					PutString(" <id> <y/n>\"\n");
				}
			}
			else if (StringSim(command, NET_RECEIVE)) {
				buff = StringSep(buff, arg1);
				Receive(StringToNum(arg1), netbuffer, NET_BUFFER_SIZE);
				PutInt(StringToNum(arg1));
				PutString(" : ");
				PutString(netbuffer);
				PutString("\n");
			}
			else if (StringSim(command, NET_RECEIVE_FTP)) {
				buff = StringSep(buff, arg1);
				StringSep(buff, arg2);
				GetFile(StringToNum(arg1), arg2);
				PutInt(StringToNum(arg1));
				PutString(" : received file \"");
				PutString(arg2);
				PutString("\"\n");
			}
			else if (StringSim(command, NET_SEND)) {
				buff = StringSep(buff, arg1);
				Send(StringToNum(arg1), buff);
			}
			else if (StringSim(command, NET_SEND_FTP)) {
				buff = StringSep(buff, arg1);
				StringSep(buff, arg2);
				SendFile(StringToNum(arg1), arg2);
			}
			else if (StringSim(command, NET_WRITE)) {
				if (isNetMessage) {
					buff = StringSep(buff, arg1);
					Send(StringToNum(arg1), buff);
				}
				else {
					PutString("Not messaging, use \"");
					PutString(NET_MESSAGE);
					PutString("\"\n");
				}
			}
			else if (StringSim(command, READ_FILE)) {
				StringSep(buff, arg1);
				fid = Open(arg1);
				if (fid) {
					Read(buffer, NET_BUFFER_SIZE, fid);
					Close(fid);
					PutString(buffer);
					PutString("READ\n");
				}
			}
			else if (StringSim(command, REMOVE_DIRECTORY)) {
				StringSep(buff, arg1);
				result = RemoveDirectory(arg1);
				if (! result) {
					PutString("Could not remove directory\n");
				}
			}
			else if (StringSim(command, REMOVE_FILE)) {
				StringSep(buff, arg1);
				result = Remove(arg1);
				if (! result) {
					PutString("Could not remove file\n");
				}
			}
			else if (StringSim(command, WRITE_FILE)) {
				StringSep(buff, arg1);
				buff = StringSep(buff, arg1);
				fid = Open(arg1);
				if (fid) {
					Write(buff, NET_BUFFER_SIZE, fid);
					Close(fid);
					PutString("WROTE\n");
				}
			}
			else {
				newProc = ForkExec (command);
				ProcJoin (newProc);
			}
		}
	}

	return 0;
}
