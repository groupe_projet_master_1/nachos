#include "syscall.h"

#define FILE_NAME "file"
#define ALPHABET "abcdefghijklmnopqrstuvwxyz"
#define LENGTH 26

/**
 * Creates a file with the name in FILE_NAME.
 * Opens it, writes the alphabet and closes it.
 * Opens it again to initialise the file seeker.
 * Reads in it, closes it and removes it.
 * Displays what was read onto the screen.
 */
int main ()
{
    OpenFileId fid;
    int n;
    char buffer[LENGTH];

    Create(FILE_NAME, 1024);
    
    fid = Open(FILE_NAME);
    Write(ALPHABET, LENGTH, fid);
    Close(fid);

    fid = Open(FILE_NAME);
    n = Read(buffer, LENGTH, fid);
    PutString("Read ");
    PutInt(n);
    PutString(" characters\n");
    Close(fid);

    Remove(FILE_NAME);

    PutString(buffer);
    PutString("\n");

    return 0;
}
