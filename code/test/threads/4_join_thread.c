#include "syscall.h"


void thread_esclave(void *i) {
    ThreadExit((int)i * 10 + 5);
}

int main() {
    PutString("Debut\n");
    int arg[5][2];
    int i;
    for(i = 0 ; i < 5 ; i++){
    	arg[i][0] = i*10 + 5;
    	arg[i][1] = ThreadCreate(thread_esclave,(void *)i);
    }
    for(i = 0 ; i < 5 ; i++){
    	if(arg[i][1] != -1){
	    	PutString("on attendait : ");
	    	PutInt(arg[i][0]);
	    	PutString(", on a recu : ");
	    	PutInt(ThreadJoin(30));
	    	PutChar('\n');
	    }
    }
    PutString("Fin\n");
    return 0;
}