#include "syscall.h"

void thread_ception(void *i) {
    int n = (int) i;
    thread_t t_id_1 = -1;
    thread_t t_id_2 = -1;
    PutInt(n);
    if (n > 0) {
        n--;
        t_id_1 = ThreadCreate(thread_ception, (void *) n);
        t_id_2 = ThreadCreate(thread_ception, (void *) n);

        //if(t_id_1 != -1){
            ThreadJoin(t_id_1);
        //}
        //if(t_id_2 != -1){
            ThreadJoin(t_id_2);
        //}
    }
    ThreadExit(0);
}

int main() {

    //16 threads au total
    thread_t t_id = ThreadCreate(thread_ception, (void *)4);

    if (t_id == -1) {
        PutString("Echec de création du premier thread\n");
        return -1;
    }

    ThreadJoin(t_id);

    PutString("Fin de programme\n");

    return 0;
}