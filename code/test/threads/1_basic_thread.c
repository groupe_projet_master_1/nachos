#include "syscall.h"

void put(void *c) {
    PutString("Thread : je suis le thread, je vais faire plusieur affichages a la suite pour montrer un entrelacement (si possible)\n");
    PutString("Thread : j'ai recu un argument de la part de mon créateur\n");
    PutString("Thread : le voici d'ailleurs : ");
    PutInt((int)c);
    PutChar('\n');
    PutString("Thread : je vais enfin retourner quelque chose, par exemple l'entier 42 ! \n");
    ThreadExit(42);
}

//Vérifie la bonne création d'un thread
int main() {
    int c = 21;
    int id;
    id = ThreadCreate(&put, (void *) c);
    PutString("Main : j'ai créé un thread\n");
    PutString("Main : je lui ai donne pour mission de parler et d'afficher l'argument que je lui ai donne\n");
    PutString("Main : l'argument etait : ");
    PutInt(c);
    PutChar('\n');
    PutString("Main : je vais attendre qu'il finisse\n");
    int ret = ThreadJoin(id);
    PutString("Main : son retour est : ");
    PutInt(ret);
    PutChar('\n');
    return 0;
}
