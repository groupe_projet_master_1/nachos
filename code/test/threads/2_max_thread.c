#include "syscall.h"

void thread_slave(void *i) {
    int n = (int) i;
    n++;
    PutInt(n);
    PutChar('\n');
}

int main() {
    PutString("Debut\n");
    int i;
    for(i = 0 ; i < 40 ; i++){
        if (ThreadCreate(thread_slave, (void *)i) == -1) {
            PutString("Trop de thread créé\n");
        }
        Yield();
    }
    PutInt(i);
    PutChar('\n');
    PutString("Fin\n");

    return 0;
}