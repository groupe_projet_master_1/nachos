// nettest.cc 
//	Test out message delivery between two "Nachos" machines,
//	using the Post Office to coordinate delivery.
//
//	Two caveats:
//	  1. Two copies of Nachos must be running, with machine ID's 0 and 1:
//		./nachos -m 0 -o 1 &
//		./nachos -m 1 -o 0 &
//
//	  2. You need an implementation of condition variables,
//	     which is *not* provided as part of the baseline threads 
//	     implementation.  The Post Office won't work without
//	     a correct implementation of condition variables.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"

#include "system.h"
#include "network.h"
#include "post.h"
#include "interrupt.h"

// Test out message delivery, by doing the following:
//	1. send a message to the machine with ID "farAddr", at mail box #0
//	2. wait for the other machine's message to arrive (in our mailbox #0)
//	3. send an acknowledgment for the other machine's message
//	4. wait for an acknowledgement from the other machine to our 
//	    original message

void
MailTest(int farAddr)
{
    PacketHeader outPktHdr, inPktHdr;
    MailHeader outMailHdr, inMailHdr;
    const char *data = "Hello there!";
    const char *ack = "Got it!";
    char buffer[MaxMailSize];

    // construct packet, mail header for original message
    // To: destination machine, mailbox 0
    // From: our machine, reply to: mailbox 1
    outPktHdr.to = farAddr;		
    outMailHdr.to = 0;
    outMailHdr.from = 0;
    outMailHdr.length = strlen(data) + 1;

    // Send the first message
    postOffice->Send(outPktHdr, outMailHdr, data); 

    // Wait for the first message from the other machine
    postOffice->Receive(0, &inPktHdr, &inMailHdr, buffer);
    printf("Got \"%s\" from %d, box %d\n",buffer,inPktHdr.from,inMailHdr.from);
    fflush(stdout);

    // Send acknowledgement to the other machine (using "reply to" mailbox
    // in the message that just arrived
    outPktHdr.to = inPktHdr.from;
    outMailHdr.to = inMailHdr.from;
    outMailHdr.length = strlen(ack) + 1;
    postOffice->Send(outPktHdr, outMailHdr, ack); 

    // Wait for the ack from the other machine to the first message we sent.
    postOffice->Receive(0, &inPktHdr, &inMailHdr, buffer);
    printf("Got \"%s\" from %d, box %d\n",buffer,inPktHdr.from,inMailHdr.from);
    fflush(stdout);

    // Then we're done!
    interrupt->Halt();
}

/*
void
MailTest(int farAddr)
{
    PacketHeader outPktHdr, inPktHdr;
    MailHeader outMailHdr, inMailHdr;
    const char *data = "Hello there!";
    const char *ack = "Got it!";
    char buffer[MaxMailSize];

    // construct packet, mail header for original message
    // To: destination machine, mailbox 0
    // From: our machine, reply to: mailbox 1
    outPktHdr.to = farAddr;		
    outMailHdr.to = 0;
    outMailHdr.from = 1;
    outMailHdr.length = strlen(data) + 1;

    // Send the first message
    postOffice->Send(outPktHdr, outMailHdr, data); 

    // Wait for the first message from the other machine
    postOffice->Receive(0, &inPktHdr, &inMailHdr, buffer);
    printf("Got \"%s\" from %d, box %d\n",buffer,inPktHdr.from,inMailHdr.from);
    fflush(stdout);

    // Send acknowledgement to the other machine (using "reply to" mailbox
    // in the message that just arrived
    outPktHdr.to = inPktHdr.from;
    outMailHdr.to = inMailHdr.from;
    outMailHdr.length = strlen(ack) + 1;
    postOffice->Send(outPktHdr, outMailHdr, ack); 

    // Wait for the ack from the other machine to the first message we sent.
    postOffice->Receive(1, &inPktHdr, &inMailHdr, buffer);
    printf("Got \"%s\" from %d, box %d\n",buffer,inPktHdr.from,inMailHdr.from);
    fflush(stdout);

    // Then we're done!
    interrupt->Halt();
}
*/

void 
MailACKTest(int farAddr){
    NetworkAddress myAddr = postOffice->getNetAddr();
    PacketHeader outPktHdr, inPktHdr;
    MailHeader outMailHdr, inMailHdr;
    const char *data = "Hello there!";
    //const char *ack = "Got it!";
    char buffer[MaxMailSize]; 

    // construct packet, mail header for original message
    // To: destination machine, mailbox 0
    // From: our machine, reply to: mailbox 1
    outPktHdr.to = farAddr;		
    outMailHdr.to = farAddr;
    outMailHdr.from = myAddr;
    outMailHdr.length = strlen(data) + 1;

    // Send the first message
    if(!postOffice->SecureSend(outPktHdr, outMailHdr, data))
    {
        printf("\nProblème lors de l'envoi du message\n");
    }

    postOffice->Receive(farAddr, &inPktHdr, &inMailHdr, buffer);
    printf("Got \"%s\" from %d, box %d\n",buffer,inPktHdr.from,inMailHdr.from);
    fflush(stdout);
    
    interrupt->Halt();
}

void 
TestBufferReceip(int farAddr)
{
    NetworkAddress myAddr = postOffice->getNetAddr();
    PacketHeader outPktHdr, inPktHdr;
    MailHeader outMailHdr, inMailHdr;
    const char *data = "Hello there! J'essai d'envoyer un message long, je ne sais pas combien de caractere je dois ecrire ! loooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooool";
    //const char *ack = "Got it!";
    char buffer[divRoundUp(strlen(data), MaxMailSize)*MaxMailSize]; 
    for(int i = 0; i< 10; i++)
    {
        if(farAddr == 0)
        {
            // construct packet, mail header for original message
            // To: destination machine, mailbox 0
            // From: our machine, reply to: mailbox 1
            outPktHdr.to = farAddr;		
            outMailHdr.to = farAddr;
            outMailHdr.from = myAddr;
            outMailHdr.length = strlen(data) + 1;

            // Send the first message
            if(!postOffice->SecureSend(outPktHdr, outMailHdr, data))
            {
                printf("\nProblème lors de l'envoi du message\n");
            }
        } else 
        {
            postOffice->Receive(farAddr, &inPktHdr, &inMailHdr, buffer);
            printf("Got \"%s\" from %d, box %d\n",buffer,inPktHdr.from,inMailHdr.from);
            fflush(stdout);
            
            /*postOffice->Receive(2, &inPktHdr, &inMailHdr, buffer);
            printf("Got \"%s\" from %d, box %d\n",buffer,inPktHdr.from,inMailHdr.from);
            fflush(stdout);*/
        }
    }
    
    interrupt->Halt();
}

void 
MailSplitSendTest(int farAddr){
    NetworkAddress myAddr = postOffice->getNetAddr();
    PacketHeader outPktHdr, inPktHdr;
    MailHeader outMailHdr, inMailHdr;
    const char *data = "Hello there! J'essai d'envoyer un message long, je ne sais pas combien de caractere je dois ecrire ! loooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooool";
    /*int size = 2*MaxMailSize + 5;
    char *data = new char[size];
    int i;
    for(i = 0; i < size; i++)
    {
        data[i] = 'u';
    }
    data[size-1] = '\0';
    //const char *ack = "Got it!";*/
    printf("\nsizeof data = %d max = %d\n", strlen(data), MaxMailSize);
    
    char buffer[divRoundUp(strlen(data), MaxMailSize)*MaxMailSize]; 
    //char buffer[1];
    // construct packet, mail header for original message
    // To: destination machine, mailbox 0
    // From: our machine, reply to: mailbox 1
    outPktHdr.to = farAddr;		
    outMailHdr.to = farAddr;
    outMailHdr.from = myAddr;
    outMailHdr.length = strlen(data) + 1;

    // Send the first message
    postOffice->SecureSend(outPktHdr, outMailHdr, data); 

    postOffice->Receive(farAddr, &inPktHdr, &inMailHdr, buffer);
    printf("Got \"%s\" from %d, box %d\n",buffer,inPktHdr.from,inMailHdr.from);
    fflush(stdout);
    
    interrupt->Halt();
}

void
ServerTest(int farAddr)
{
    //On lance juste le thread sans jamais l'arrêter 
}

void 
SendFileTest(int farAddr)
{
    NetworkAddress myAddr = postOffice->getNetAddr();
    PacketHeader outPktHdr;//, inPktHdr;
    MailHeader outMailHdr;//, inMailHdr;
    const char *fileName = "test.txt";
    //const char *ack = "Got it!";
    if(farAddr == 0)
    {
        // construct packet, mail header for original message
        // To: destination machine, mailbox 0
        // From: our machine, reply to: mailbox 1
        outPktHdr.to = farAddr;		
        outMailHdr.to = farAddr;
        outMailHdr.from = myAddr;
        outMailHdr.length = strlen(fileName) + 1;

        // Send the first message
        if(!postOffice->SendFile(outPktHdr, outMailHdr, fileName))
        {
            printf("\nProblème lors de l'envoi du fichier\n");
        }

        interrupt->Halt();
    } else 
    {
    }
}
