#include "mailtampon.h"
#include "post.h"

//---------------------------------------------------------------------------------------------------------
//
// Fonction de Mail Tampon 
//
//---------------------------------------------------------------------------------------------------------

MailTampon::MailTampon()
{
    lock = new Lock("mail tampon");
    messages = new SynchList();
}

MailTampon::~MailTampon()
{
    delete messages;
    delete lock;
}

/**
 * Retourne vrai si il y a au moins un message dans le tampon
*/
bool
MailTampon::IsMessage()
{
    return !messages->IsEmpty();
}

/**
 * Vérrouille le lock
*/
void 
MailTampon::Acquire()
{
    lock->Acquire();
}

/**
 * Libére le lock
*/
void 
MailTampon::Release()
{
    lock->Release();
}

/**
 * Renvoi l'id du dernier message dans le tampon
*/
unsigned 
MailTampon::IdMail()
{
    unsigned ret;
    Mail *mail = (Mail *) messages->GetLast();
    ret = mail->mailHdr.idPID;
    return ret;
}

/**
 * Renvoi l'offset du dernier message dans le tampon
*/
unsigned 
MailTampon::OffsetMail()
{
    unsigned ret;
    Mail *mail = (Mail *) messages->GetLast();
    ret = mail->mailHdr.offSet;
    return ret;
}

/**
 * Retourne vrai si le message contenu dans le tampon est complet
*/
bool 
MailTampon::IsMessageComplete()
{
    bool ret;
    Mail *mail = (Mail *) messages->GetLast();
    ret = mail->mailHdr.offSet == (mail->mailHdr.nbMessage - 1) ;
    return ret;
}

/**
 * Renvoi le premier message contenu dans le tampon
*/
Mail
*MailTampon::GetMail()
{
    Mail *mail = (Mail *) messages->Remove();
    return mail;
}

/**
 * Ajoute un message à la fin du tampon
*/
void 
MailTampon::AjouterDonnee(PacketHeader pktHdr, MailHeader mailHdr, char *data)
{
    Mail *mail = new Mail(pktHdr, mailHdr, data);
    messages->Append((void *)mail);
}