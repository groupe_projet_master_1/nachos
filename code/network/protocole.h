#ifndef __PROTOCOLE_H__
#define __PROTOCOLE_H__

class PostOffice;
class PacketHeader;
class MailHeader;
class Protocole {
    public:
        Protocole(PostOffice *office){
            myPostOffice = office;
            idMessage = 1;
        };
        virtual ~Protocole() {
        };
        virtual void SendACK(PacketHeader *pktHdr, MailHeader *mailHdr) =0; //fonction pour envoyer un acquittement 
        virtual bool SecureSend(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data, int type)  =0;
        virtual void HandleMessage(MailHeader mailHdr, PacketHeader pktHdr, char *buffer) =0;
        virtual bool CloseConnexion(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data) =0;
        virtual bool OpenConnexion(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data) =0;
        virtual void CloseAllConnexion() =0;
        virtual int SplitSend(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data, int type) =0;
        virtual void Receive(int box, PacketHeader *pktHdr, MailHeader *mailHdr, char* data) =0;

//------------------------------------------------------------------------------------------------------------------------
//
//Pour l'envoie de fichier
//
//------------------------------------------------------------------------------------------------------------------------
        virtual int SendFile(PacketHeader *pktHdr, MailHeader *mailHdr, const char *fileName) = 0;
        virtual int GetFile(PacketHeader *pktHdr, MailHeader *mailHdr, const char *fileName) = 0;
    protected:
        PostOffice *myPostOffice;
        unsigned idMessage;
};
#endif