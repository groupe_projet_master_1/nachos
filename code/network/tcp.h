#ifndef __TCP_H__
#define __TCP_H__
#include "bitmap.h"
#include "protocole.h"

//Fonctions appelée par les timer 
extern void TimerWaitSendEnd(int n);
extern void TimerWaitClose(int n);
extern void TimerWait(int n);

class PostOffice;
class PacketHeader;
class MailHeader;

/**
 * Protocole de simulation de TCP
*/
class OwnTCP: public Protocole {
    public:
        OwnTCP(PostOffice *office);
        virtual ~OwnTCP();
        void SendACK(PacketHeader *pktHdr, MailHeader *mailHdr); //Fonction pour envoyer un acquitment
        bool SecureSend(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data, int type); //Envoie fiable d'un message
        void HandleMessage(MailHeader mailHdr, PacketHeader pktHdr, char *buffer); //Gestion de la réception de message
        bool CloseConnexion(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data); //Fermeture de connexion 
        bool OpenConnexion(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data); //Ouverture de connexion 
        void CloseAllConnexion(); //Fermeture de toutes les connexions 
        int SplitSend(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data, int type); //Envoie d'un gros message
        void Receive(int box, PacketHeader *pktHdr, MailHeader *mailHdr, char* data); //Lecture d'un message réceptionné
        void SendACKConnexion(PacketHeader *pktHdr, MailHeader *mailHdr); //Envoie d'un acquitment de connexion
        int SendFile(PacketHeader *pktHdr, MailHeader *mailHdr, const char *fileName); //inutilisé dans tcp
        int GetFile(PacketHeader *pktHdr, MailHeader *mailHdr, const char *fileName);
};

#endif