// post.h 
//	Data structures for providing the abstraction of unreliable,
//	ordered, fixed-size message delivery to mailboxes on other 
//	(directly connected) machines.  Messages can be dropped by
//	the network, but they are never corrupted.
//
// 	The US Post Office delivers mail to the addressed mailbox. 
// 	By analogy, our post office delivers packets to a specific buffer 
// 	(MailBox), based on the mailbox number stored in the packet header.
// 	Mail waits in the box until a thread asks for it; if the mailbox
//      is empty, threads can wait for mail to arrive in it. 
//
// 	Thus, the service our post office provides is to de-multiplex 
// 	incoming packets, delivering them to the appropriate thread.
//
//      With each message, you get a return address, which consists of a "from
// 	address", which is the id of the machine that sent the message, and
// 	a "from box", which is the number of a mailbox on the sending machine 
//	to which you can send an acknowledgement, if your protocol requires 
//	this.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"

#ifndef POST_H
#define POST_H

#include "network.h"
#include "synchlist.h"
#include "bitmap.h"

#include "tcp.h"
#include "ftp.h"
#include "protocole.h"
#include "mailtampon.h"
//class Protocole;

#define NBOXES 10

// Mailbox address -- uniquely identifies a mailbox on a given machine.
// A mailbox is just a place for temporary storage for messages.
typedef int MailBoxAddress;

// The following class defines part of the message header.  
// This is prepended to the message by the PostOffice, before the message 
// is sent to the Network.

//Essaye de s'inspirer des ACK dans TCP


class MailHeader {
  public:
    enum TypeMessage {
      ACK = 0,
      MESSAGE = 1,
      CONNEXION = 2,
      CLOSED = 3,
      FILEGET = 4, //demande de reception de fichier 
      FILESEND = 5, //demande d'envoi de fichier
      ACKCONNEXION = 6,
      ERROR = 7,
      ENDFILE = 8,
      FILESENDEND = 9
    };
    MailBoxAddress to;		// Destination mail box
    MailBoxAddress from;	// Mail box to reply to
    unsigned length;		// Bytes of message data (excluding the 
				// mail header)
    unsigned idPIDSender;     //Contient l'id du message ayant demandé un fichier
    unsigned idPID;          //tous les messages qui ont le même id doivent être assemblé
    unsigned offSet;         //Indique le numéro du message
    unsigned nbMessage;     //Le nombre de message composant le data
    TypeMessage type;   //type du message 
};

// Maximum "payload" -- real data -- that can included in a single message
// Excluding the MailHeader and the PacketHeader

#define MaxMailSize 	(MaxPacketSize - sizeof(MailHeader))


// The following class defines the format of an incoming/outgoing 
// "Mail" message.  The message format is layered: 
//	network header (PacketHeader) 
//	post office header (MailHeader) 
//	data

class Mail {
  public:
     Mail(PacketHeader pktH, MailHeader mailH, char *msgData);
				// Initialize a mail message by
				// concatenating the headers to the data

     PacketHeader pktHdr;	// Header appended by Network
     MailHeader mailHdr;	// Header appended by PostOffice
     char data[MaxMailSize];	// Payload -- message data
};

// The following class defines a single mailbox, or temporary storage
// for messages.   Incoming messages are put by the PostOffice into the 
// appropriate mailbox, and these messages can then be retrieved by
// threads on this machine.

class MailBox {
  public: 
    enum EtatConnexion {
      OPEN = 0, //La connexion est ouverte
      WAITCLOSE = 1, //La connexion est en train de se fermer
      CLOSE = 2, //la connexion est fermée
      WAITSENDEND = 3, //attente pour envoyer le message END 
      WAIT = 4
    };
    MailBox();			// Allocate and initialize mail box
    ~MailBox();			// De-allocate mail box

    void Put(PacketHeader pktHdr, MailHeader mailHdr, char *data);
   				// Atomically put a message into the mailbox
    void Get(PacketHeader *pktHdr, MailHeader *mailHdr, char *data); 
    //bool GetACK(PacketHeader *pktHdr, MailHeader *mailHdr, char *data) ;
   				// Atomically get a message out of the 
				// mailbox (and wait if there is no message 
				// to get!)
    void ConnexionOppened();//Ouvre la connexion
    void ConnexionClosed();//ferme la connexion
    void ConnexionWaitClose();//met la connexion sur l'etat WAITCLOSE
    void ConnexionWaitSendEnd();//met la connexion sur l'etat WAITSENDEND
    bool ConnexionIsOpen(); //renvoie vrai si la connexion est ouverte
    bool ConnexionIsClose(); //retourne vrai si la connexion est fermée
    EtatConnexion StateConnexion();//retourne l'état de la connexion
    bool IsEmpty(); //retourne vrai si il n'y a pas de message dans la boite aux lettres
    void TimerEnleverMessage(unsigned currentMessage, unsigned offset); //fonction pour que le timerr enlève un message
    void AjouterMessage(PacketHeader pktHdr, MailHeader mailHdr, char *data, int numBox); //ajout d'un message dans le tampon
    EtatConnexion GetConnexion(){
      return connexionState;
    }
    void SetConnexion(EtatConnexion e){
      connexionState = e;
    }
    MailTampon *GetTampon()
    {
      return tampon;
    }
    SynchList *GetMessages()
    {
      return messages;
    }
  private:
    MailTampon *tampon; //Class tampon qui sert à vérifier les messages entrant
    SynchList *messages;	// A mailbox is just a list of arrived messages
    EtatConnexion connexionState; //vrai si connexion est établie
};

// The following class defines a "Post Office", or a collection of 
// mailboxes.  The Post Office is a synchronization object that provides
// two main operations: Send -- send a message to a mailbox on a remote 
// machine, and Receive -- wait until a message is in the mailbox, 
// then remove and return it.
//
// Incoming messages are put by the PostOffice into the 
// appropriate mailbox, waking up any threads waiting on Receive.

#define TEMPO 10000000
#define MAXREEMISSIONS 20

class PostOffice {
  public:
    enum TypeProtocole {
      TCP = 0,
      NORMAL = 1,
      FTP = 2
    };
    PostOffice(NetworkAddress addr, double reliability, int nBoxes, TypeProtocole type);
				// Allocate and initialize Post Office
				//   "reliability" is how many packets
				//   get dropped by the underlying network
    ~PostOffice();		// De-allocate Post Office data
    
    
    void Send(PacketHeader pktHdr, MailHeader mailHdr, const char *data);// Send a message to a mailbox on a remote 
				// machine.  The fromBox in the MailHeader is 
				// the return box for ack's.
    int SecureSend(PacketHeader pktHdr, MailHeader mailHdr, const char* data); //Envoie "fiable" d'un message
    void SendError(PacketHeader pktHdr, MailHeader mailHdr, const char* data); //Envoie un message d'erreur
    int SendFile(PacketHeader pktHdr, MailHeader mailHdr, const char* fileName); //Envoie un fichier
    int GetFile(PacketHeader pktHdr, MailHeader mailHdr, const char* fileName); //Demande l'envoie d'un fichier

    void Receive(int box, PacketHeader *pktHdr, 
		MailHeader *mailHdr, char *data);
    				// Retrieve a message from "box".  Wait if
				// there is no message in the box.

    void PostalDelivery();	// Wait for incoming messages, 
				// and then put them in the correct mailbox

    void PacketSent();		// Interrupt handler, called when outgoing 
				// packet has been put on network; next 
				// packet can now be sent
    void IncomingPacket();	// Interrupt handler, called when incoming
   				// packet has arrived and can be pulled
				// off of network (i.e., time to call 
				// PostalDelivery)
    NetworkAddress getNetAddr(){
      return netAddr;
    }
    MailBox *getBoxes(){
      return boxes;
    }
    int getNumBox(){
      return numBoxes;
    }
    void MarkACKReceip(int indice);
    void ClearACKReceip(int indice);
    bool TestACKReceip(int indice);
    Protocole *GetProtocole()
    {
      return protocole;
    }
  private:
    Network *network;		// Physical network connection
    NetworkAddress netAddr;	// Network address of this machine
    MailBox *boxes;		// Table of mail boxes to hold incoming mail
    int numBoxes;		// Number of mail boxes
    Semaphore *messageAvailable;// V'ed when message has arrived from network
    Semaphore *messageSent;	// V'ed when next message can be sent to network
    Lock *sendLock;		// Only one outgoing message at a time
    
    Lock *ackReceipLock;
    BitMap *ackReceip;
    TypeProtocole typeProtocole;
    Protocole *protocole;
};

#endif
