#include "ftp.h"
#include "post.h"
#include "interrupt.h"
#include "system.h" 
#include "utility.h"
#include "openfile.h"
FileServer FileServer::m_instance = FileServer();
static FileServer *fileServer = &FileServer::Instance();
//Pour la récéption de fichier
static Semaphore *fileAvailable = new Semaphore("file available", 0);
//Pour l'envoie de fichier
static Semaphore *sendFile = new Semaphore("file send", 0);

/**
 * Méthode du singleton pour FileServer
*/
FileServer& 
FileServer::Instance(void){
    return m_instance;
}

//-----------------------------------------------------------------------------------------------------------
//
// Gestionnaire box et tampon pour fichier
//
//-----------------------------------------------------------------------------------------------------------

/**
 * Récupère un message dans la boite aux lettres du receive
*/
void 
FileServer::GetFileReceive(PacketHeader *pktHdr, MailHeader *mailHdr, char *data, int numBox) 
{ 
    DEBUG('n', "Waiting for mail in mailbox\n");
    Mail *mail = (Mail *) boxesFileReceive[numBox].GetMessages()->Remove();	// remove message from list;
						// will wait if list is empty

    *pktHdr = mail->pktHdr;
    *mailHdr = mail->mailHdr;
    bcopy(mail->data, data, mail->mailHdr.length);
					// copy the message data into
					// the caller's buffer
    delete mail;			// we've copied out the stuff we
					// need, we can now discard the message
}

void
CopyMessage(PacketHeader p1, MailHeader h1, char *buffer1, PacketHeader p2, MailHeader h2, char *buffer2)
{
    p1.from = p2.from;
    p1.length = p2.length;
    p1.to = p2.to;

    h1.to = h2.to;	
    h1.from = h2.from;	
    h1.length = h2.length;
    h1.idPIDSender = h2.idPIDSender;   
    h1.idPID = h2.idPID;         
    h1.offSet = h2.offSet;  
    h1.nbMessage = h2.nbMessage;   
    h1.type = h2.type;

    bcopy(buffer2, buffer1, MaxMessageSize);    
}

void 
FileServer::Find3MessagesFileReceive(PacketHeader *inPktHdr1, MailHeader *inMailHdr1, char *buffer1, PacketHeader *inPktHdr2, MailHeader *inMailHdr2, char *buffer2, PacketHeader *inPktHdr3, MailHeader *inMailHdr3, char *buffer3, int numBox)
{
    
    //On récupére le nom du fichier
    ReceiveFileReceive(inPktHdr1, inMailHdr1, buffer1, numBox);
    
    //On récupére le nom du fichier
    ReceiveFileReceive(inPktHdr2, inMailHdr2, buffer2, numBox);
    
    //On récupére le nom du fichier
    ReceiveFileReceive(inPktHdr3, inMailHdr3, buffer3, numBox);
    while(inMailHdr1->idPID + 2 != inMailHdr3->idPID)
    {
        if(inMailHdr2->idPID + 1 == inMailHdr3->idPID)
        {
            CopyMessage(*inPktHdr1, *inMailHdr1, buffer1, *inPktHdr2, *inMailHdr2, buffer2);
            CopyMessage(*inPktHdr2, *inMailHdr2, buffer2, *inPktHdr3, *inMailHdr3, buffer3);
            ReceiveFileReceive(inPktHdr3, inMailHdr3, buffer3, numBox);
        } else {
            CopyMessage(*inPktHdr1, *inMailHdr1, buffer1, *inPktHdr2, *inMailHdr2, buffer2);
            ReceiveFileReceive(inPktHdr2, inMailHdr2, buffer2, numBox);
            ReceiveFileReceive(inPktHdr3, inMailHdr3, buffer3, numBox);
        }
    }
}

/**
 * Récupère un message dans la boite aux lettres du get
*/
void 
FileServer::GetFileGet(PacketHeader *pktHdr, MailHeader *mailHdr, char *data, int numBox) 
{ 
    DEBUG('n', "Waiting for mail in mailbox\n");
    Mail *mail = (Mail *) boxesFileGet[numBox].GetMessages()->Remove();	// remove message from list;
						// will wait if list is empty

    *pktHdr = mail->pktHdr;
    *mailHdr = mail->mailHdr;
    bcopy(mail->data, data, mail->mailHdr.length);
					// copy the message data into
					// the caller's buffer
    delete mail;			// we've copied out the stuff we
					// need, we can now discard the message
}

/**
 * Récupère un message dans la boite aux lettres des messages de retour
*/
int 
FileServer::GetFileReturn(unsigned idMail, int numBox) 
{ 
    DEBUG('n', "Waiting for mail in mailbox\n");
    MailRetour *mail = (MailRetour *) boxesMessageRetour[numBox].GetMessages()->Remove();	// remove message from list;
						// will wait if list is empty
    int retCode;
    //On vérifie que le message dans la boite n'est pas postérieur au notre
    if(mail->idMessage == idMail)
    {
        //C'est bien le mail que l'on voulait recevoir
        retCode = mail->retCode;
        delete mail; 
        return retCode;
    } else if(mail->idMessage < idMail)
    {
        //Ce mail peut être supprimé
        delete mail;
        while(!boxesMessageRetour[numBox].GetMessages()->IsEmpty())
        {
            mail = (MailRetour *) boxesMessageRetour[numBox].GetMessages()->Remove();
            if(mail->idMessage == idMail)
            {
                retCode = mail->retCode;
                delete mail;
                return retCode;
            } else if (mail->idMessage < idMail)
            {
                delete mail;
            } else 
            {
                boxesMessageRetour[numBox].GetMessages()->Append((void *)mail);
                return ERR_NO_ERROR;
            }
        }
    } else 
    {
        boxesMessageRetour[numBox].GetMessages()->Append((void *)mail);
        return ERR_NO_ERROR;
    }
    return 1;
}

/**
 * Met un message dans la boites aux lettres du receive
*/
void 
FileServer::PutFileReceive(PacketHeader pktHdr, MailHeader mailHdr, char *data, int numBox)
{ 
    Mail *mail = new Mail(pktHdr, mailHdr, data); 

    boxesFileReceive[numBox].GetMessages()->Append((void *)mail);	// put on the end of the list of 
					// arrived messages, and wake up 
					// any waiters
}

/**
 * Met un message dans la boites aux lettres du get
*/
void 
FileServer::PutFileGet(PacketHeader pktHdr, MailHeader mailHdr, char *data, int numBox)
{ 
    Mail *mail = new Mail(pktHdr, mailHdr, data); 

    boxesFileGet[numBox].GetMessages()->Append((void *)mail);	// put on the end of the list of 
					// arrived messages, and wake up 
					// any waiters
}

/**
 * Met un message dans la boites aux lettres des messages de retour 
*/
void 
FileServer::PutFileReturn(int returnCode, unsigned idMail, int numBox)
{ 
    MailRetour *mailretour = new MailRetour(returnCode, idMail); 

    boxesMessageRetour[numBox].GetMessages()->Append((void *)mailretour);	// put on the end of the list of 
					// arrived messages, and wake up 
					// any waiters
}

/**
 * Renvoie vrai si la liste des messages de retour est vide
*/
bool 
FileServer::IsEmptyReturn(int numBox)
{
    return boxesMessageRetour[numBox].GetMessages()->IsEmpty();
}

/**
 * Timer qui enlève un message du tampon receive si il est la depuis trop longtemps
*/
void
TimerRemoveMessageFileReceive(int n)
{
    OwnTimer *t = (OwnTimer*) n;
    fileServer->TimerEnleverMessageFileReceive(t->currentMessage, t->offset, t->numBox);
    delete t;
}

/**
 * Timer qui enlève un message du tampon get si il est la depuis trop longtemps
*/
void
TimerRemoveMessageFileGet(int n)
{
    OwnTimer *t = (OwnTimer*) n;
    fileServer->TimerEnleverMessageFileGet(t->currentMessage, t->offset, t->numBox);
    delete t;
}

/**
 * Fonction pour enlever le message du tampon receive via le timer
*/
void 
FileServer::TimerEnleverMessageFileReceive(unsigned currentMessage, unsigned offset, int numBox)
{
    boxesFileReceive[numBox].GetTampon()->Acquire();
    if(boxesFileReceive[numBox].GetTampon()->IsMessage() && currentMessage == boxesFileReceive[numBox].GetTampon()->IdMail() && offset == boxesFileReceive[numBox].GetTampon()->OffsetMail())
    {
        if(boxesFileReceive[numBox].GetTampon()->IsMessageComplete())
        {
            while(boxesFileReceive[numBox].GetTampon()->IsMessage())
            {
                //Il y a un message il faut l'enlever 
                Mail *mailTampon = boxesFileReceive[numBox].GetTampon()->GetMail();
                //On l'ajoute à la liste des messages
                boxesFileReceive[numBox].GetMessages()->Append((void *)mailTampon);
            }
            IncrementeCompteurMessageFileReceive(numBox);
        } else
        {
            //Le message n'est pas complet on supprime le contenu du tampon
            while(boxesFileReceive[numBox].GetTampon()->IsMessage())
            {
                //Il y a un message il faut l'enlever 
                Mail *mailTampon = boxesFileReceive[numBox].GetTampon()->GetMail();
                delete mailTampon;
            }
        }
    }
    boxesFileReceive[numBox].GetTampon()->Release();
}

/**
 * Fonction pour enlever le message du tampon get via le timer
*/
void 
FileServer::TimerEnleverMessageFileGet(unsigned currentMessage, unsigned offset, int numBox)
{
    boxesFileGet[numBox].GetTampon()->Acquire();
    if(boxesFileGet[numBox].GetTampon()->IsMessage() && currentMessage == boxesFileGet[numBox].GetTampon()->IdMail() && offset == boxesFileGet[numBox].GetTampon()->OffsetMail())
    {
        if(boxesFileGet[numBox].GetTampon()->IsMessageComplete())
        {
            while(boxesFileGet[numBox].GetTampon()->IsMessage())
            {
                //Il y a un message il faut l'enlever 
                Mail *mailTampon = boxesFileGet[numBox].GetTampon()->GetMail();
                //On l'ajoute à la liste des messages
                boxesFileGet[numBox].GetMessages()->Append((void *)mailTampon);
            }
        } else
        {
            //Le message n'est pas complet on supprime le contenu du tampon
            while(boxesFileGet[numBox].GetTampon()->IsMessage())
            {
                //Il y a un message il faut l'enlever 
                Mail *mailTampon = boxesFileGet[numBox].GetTampon()->GetMail();
                delete mailTampon;
            }
        }
    }
    boxesFileGet[numBox].GetTampon()->Release();
}

/**
 * Ajoute un message au tampon receive 
*/
void
FileServer::AjouterMessageFileReceive(PacketHeader pktHdr, MailHeader mailHdr, char *data, int numBox)
{
    boxesFileReceive[numBox].GetTampon()->Acquire();
    if(boxesFileReceive[numBox].GetTampon()->IsMessage())
    {
        //verifier pas meme message 
        if(mailHdr.idPID == boxesFileReceive[numBox].GetTampon()->IdMail() && mailHdr.offSet == boxesFileReceive[numBox].GetTampon()->OffsetMail())
        {
            //Meme message 
            boxesFileReceive[numBox].GetTampon()->Release();
            return;

        } else if(mailHdr.idPID != boxesFileReceive[numBox].GetTampon()->IdMail())
        {
            if(boxesFileReceive[numBox].GetTampon()->IsMessageComplete())
            {
                while(boxesFileReceive[numBox].GetTampon()->IsMessage())
                {
                    //Il y a un message il faut l'enlever 
                    Mail *mailTampon = boxesFileReceive[numBox].GetTampon()->GetMail();
                    //On l'ajoute à la liste des messages
                    boxesFileReceive[numBox].GetMessages()->Append((void *)mailTampon);
                }
                //On incrémente le compteur 
                IncrementeCompteurMessageFileReceive(numBox);
            } else
            {
                //Le message n'est pas complet on supprime le contenu du tampon
                while(boxesFileReceive[numBox].GetTampon()->IsMessage())
                {
                    //Il y a un message il faut l'enlever 
                    Mail *mailTampon = boxesFileReceive[numBox].GetTampon()->GetMail();
                    delete mailTampon;
                }
            }
        }
    }
    //On ajoute le nouveau messages
    boxesFileReceive[numBox].GetTampon()->AjouterDonnee(pktHdr, mailHdr, data);
    //Lancement du timer 
    OwnTimer *t = new OwnTimer(boxesFileReceive[numBox].GetTampon()->IdMail(), numBox, mailHdr.offSet);
    //Si on atteint le timer et que le message y est toujours on l'ajoute dans la boite aux lettres
    interrupt->Schedule(TimerRemoveMessageFileReceive, (int)t, (MAXREEMISSIONS * TEMPO * 2), TimerInt);
    boxesFileReceive[numBox].GetTampon()->Release();
}

void
FileServer::IncrementeCompteurMessageFileReceive(int numBox)
{
    lockFileReceiveCompteur->Acquire();
    fileReceiveCompteur[numBox]++;
    lockFileReceiveCompteur->Release();
}

void
FileServer::IncrementeCompteurMessageFileGet(int numBox)
{
    lockFileGetCompteur->Acquire();
    fileGetCompteur[numBox]++;
    lockFileGetCompteur->Release();
}

void 
FileServer::DecrementeCompteurMessageFileReceive(int numBox)
{
    lockFileReceiveCompteur->Acquire();
    fileReceiveCompteur[numBox]--;
    lockFileReceiveCompteur->Release();
}

void 
FileServer::DecrementeCompteurMessageFileGet(int numBox)
{
    lockFileGetCompteur->Acquire();
    fileGetCompteur[numBox]--;
    lockFileGetCompteur->Release();
}


/**
 * Ajoute un message au tampon get, retourne vrai si le message a été ajouté et qu'il est complet, faux sinon
*/
bool
FileServer::AjouterMessageFileGet(PacketHeader pktHdr, MailHeader mailHdr, char *data, int numBox)
{
    boxesFileGet[numBox].GetTampon()->Acquire();
    bool iscomplete = false;
    if(boxesFileGet[numBox].GetTampon()->IsMessage())
    {
        //verifier pas meme message 
        if(mailHdr.idPID == boxesFileGet[numBox].GetTampon()->IdMail() && mailHdr.offSet == boxesFileGet[numBox].GetTampon()->OffsetMail())
        {
            //Meme message 
            boxesFileGet[numBox].GetTampon()->Release();
            return false;;

        } else if(mailHdr.idPID != boxesFileGet[numBox].GetTampon()->IdMail())
        {
            if(boxesFileGet[numBox].GetTampon()->IsMessageComplete())
            {
                while(boxesFileGet[numBox].GetTampon()->IsMessage())
                {
                    //Il y a un message il faut l'enlever 
                    Mail *mailTampon = boxesFileGet[numBox].GetTampon()->GetMail();
                    //On l'ajoute à la liste des messages
                    boxesFileGet[numBox].GetMessages()->Append((void *)mailTampon);
                }
            } else
            {
                //Le message n'est pas complet on supprime le contenu du tampon
                while(boxesFileGet[numBox].GetTampon()->IsMessage())
                {
                    //Il y a un message il faut l'enlever 
                    Mail *mailTampon = boxesFileGet[numBox].GetTampon()->GetMail();
                    delete mailTampon;
                }
            }
        }
    }
    //On ajoute le nouveau messages
    boxesFileGet[numBox].GetTampon()->AjouterDonnee(pktHdr, mailHdr, data);
    iscomplete = boxesFileGet[numBox].GetTampon()->IsMessageComplete();
    //Lancement du timer 
    OwnTimer *t = new OwnTimer(boxesFileGet[numBox].GetTampon()->IdMail(), numBox, mailHdr.offSet);
    //Si on atteint le timer et que le message y est toujours on l'ajoute dans la boite aux lettres
    interrupt->Schedule(TimerRemoveMessageFileGet, (int)t, (MAXREEMISSIONS * TEMPO * 2), TimerInt);
    boxesFileGet[numBox].GetTampon()->Release();
    return iscomplete;
}

//------------------------------------------------------------------------------------
//
// Réception création de fichier
//
//------------------------------------------------------------------------------------
/**
 * Fonction du fork pour créer le fichier 
*/
void CreateFile(int n)
{
    fileServer->CreateFile();
}

void 
FileServer::SendFile()
{
    for(;;)
    {
        //On attend qu'un fichier arrive 
        sendFile->P();
        int i;
        for(i = 0; i < NBOXES; i++)
        {
            if(!FileCompleteGetCompteur(i))
            {  
                continue;
            }
            //On recupere le nom du fichier à envoyer
            printf("\nDemande d'envoie de fichier\n");
            PacketHeader inPktHdr;
            MailHeader inMailHdr;
            char fileName[MaxMessageSize]; 
            //On récupére le nom du fichier
            ReceiveFileGet(&inPktHdr, &inMailHdr, fileName, i);
            printf("\nfile name %s\n", fileName);
            PacketHeader outPktHdr;
            MailHeader outMailHdr;
            // Creation de l'entête
            outPktHdr.to = inPktHdr.from;
            outPktHdr.from = postOffice->getNetAddr();;
            outPktHdr.length = sizeof(MailHeader) + 1;
            //Création du mailHdr
            outMailHdr.to = inMailHdr.from;
            outMailHdr.from = inMailHdr.to;
            outMailHdr.length = strlen(fileName) + 1;
            
            //On veut savoir à quelle mail on répond
            outMailHdr.idPIDSender = inMailHdr.idPID;

            int j = postOffice->SendFile(outPktHdr, outMailHdr, fileName);
            if(j != 1)
            {
                //L'envoi a échoué, on envoit un message d'erreur contenant le code de retour
                snprintf(fileName, 2, "%d", j);
                outMailHdr.length = strlen(fileName) + 1;
                postOffice->SendError(outPktHdr, outMailHdr, fileName);
            }
        }
    }
}

void 
FileServer::CreateFile()
{
    OpenFile *newFile;
    for(;;)
    {
        //On attend qu'un fichier arrive 
        fileAvailable->P();
        int retCode;
        printf("\nReception de fichier\n");
        int i;
        for(i = 0; i < NBOXES; i++)
        {
            if(!FileCompleteReceiveCompteur(i))
            {
                continue;
            }
            PacketHeader inPktHdr, inPktHdr1, inPktHdr2;
            MailHeader inMailHdr, inMailHdr1, inMailHdr2;
            char name[MaxMessageSize];
            char sizeStr[MaxMessageSize];
            char buffer[MaxMessageSize];
            //On a un fichier dans cet boxe on cherche les 3 messages consécutifs
            Find3MessagesFileReceive(&inPktHdr, &inMailHdr, name, &inPktHdr1, &inMailHdr1, sizeStr, &inPktHdr2, &inMailHdr2, buffer, i);
            name[0] = 'e';

            int size;
            sscanf(sizeStr, "%d", &size);
            //On créé le fichier
            if(!fileSystem->Create(name, size))
            {
                //Problème lors de la création 
                printf("\nProbleme creation fichier\n");
                retCode = ERR_CREATE_FILE;
            } else {
                int fid = fileSystem->Open(name);
                newFile = fileSystem->GetOpenFile(fid);
                if(!newFile)
                {
                    //problème à l'ouverture du fichier
                    printf("\nProbleme ouverture fichier\n");
                    retCode = ERR_OPEN_FILE;
                } else {
                    if(newFile->Write(buffer, size) != size)
                    {
                        printf("\nProbleme ecriture\n");
                        //Problème lors de l'écriture des données on supprime le fichier
                    fileSystem->Remove(name);
                    retCode = ERR_WRITE_FILE;
                    } else
                    {
                        printf("\nfichier cree\n");
                        /**
                         * Retour utilisateur 
                        */
                        retCode = 1;
                    }
                }
                fileSystem->Close(fid);
            }
            if(inMailHdr.idPIDSender != 0)
            {
                //c'est un fichier dont on a demandé l'envoie
                PutFileReturn(retCode, inMailHdr.idPIDSender, i);
            }
            //On vérifie qu'on a pas un autre fichier à créer dans cette même boxe
            i--;
        }
        
    }
}

/**
 * Fonction receive pour lire les donnée dans le boite aux lettres du receive
*/
void 
FileServer::ReceiveFileReceive(PacketHeader *pktHdr, MailHeader *mailHdr, char* data, int numBox) 
{
    int nbMessageTotal;
    char *tmp;
    tmp = new char[MaxMailSize];
    GetFileReceive(pktHdr, mailHdr, tmp, numBox);
    ASSERT(mailHdr->length <= MaxMailSize);
    nbMessageTotal = mailHdr->nbMessage;
    memcpy(data, tmp, mailHdr->length);
    delete tmp;
    int i;
    //Ajout des autres morceaux si il y en a 
    for(i = 1; i < nbMessageTotal; i++)
    {
        tmp = new char[MaxMailSize];
        GetFileReceive(pktHdr, mailHdr, tmp,numBox);
        ASSERT(mailHdr->length <= MaxMailSize);
        memcpy(data + i * MaxMailSize, tmp, mailHdr->length);
        delete tmp;
    }
    DecrementeCompteurMessageFileReceive(numBox);
}

/**
 * Fonction receive pour lire les donnée dans le boite aux lettres du get
*/
void 
FileServer::ReceiveFileGet(PacketHeader *pktHdr, MailHeader *mailHdr, char* data, int numBox) 
{
    int nbMessageTotal;
    char *tmp;
    tmp = new char[MaxMailSize];
    GetFileGet(pktHdr, mailHdr, tmp, numBox);
    ASSERT(mailHdr->length <= MaxMailSize);
    nbMessageTotal = mailHdr->nbMessage;
    //On ajoute le premier morceau du message
    memcpy(data, tmp, mailHdr->length);
    delete tmp;
    int i;
    //Ajout des autres morceaux si il y en a 
    for(i = 1; i < nbMessageTotal; i++)
    {
        tmp = new char[MaxMailSize];
        GetFileGet(pktHdr, mailHdr, tmp, numBox);
        ASSERT(mailHdr->length <= MaxMailSize);
        memcpy(data + i * MaxMailSize, tmp, mailHdr->length);
        delete tmp;
    }
}

/**
 * On ouvre le fichier. Retourne true si le fichier a bien été ouvert
 * false sinon
*/
bool 
FileServer::ServerOpenFile(char *name){
    int pid = fileSystem->Open(name);
    if(pid < 0)
    {
        //Impossible d'ouvrir le fichier
        return false;
    }
    file = fileSystem->GetOpenFile(pid);
    if (file == NULL){
        //Impossible d'ouvrir le fichier
        return false;
    }
    return true;
}

/**
 * Fonction pour réallouer un tableau de taille lastSize en taille 
 * newSize
*/
void 
FileServer::Resize(size_t newSize, char *buffer, size_t lastSize){
    char *newArray = new char[newSize];
    memcpy(newArray, buffer, lastSize);
    delete [] buffer;
    buffer = newArray;
}

/**
 * Lit le contenu d'un fichier et le met dans le buffer
*/
bool 
FileServer::ServerReadFile(char *buffer)
{
    if(!file)
    {
        return false;
    }
    if(file->Read(buffer, file->Length()) != file->Length())
    {
        //On a pas réussi à lire tout le fichier
        return false;
    }
    return true;
}

int 
FileServer::SizeFile()
{
    return file->Length();
}
FileServer::FileServer(){
    boxesFileGet = new MailBox[NBOXES];
    boxesMessageRetour = new MailBox[NBOXES];
    boxesFileReceive = new MailBox[NBOXES];
    int i;
    fileGetCompteur = new int[NBOXES];
    fileReceiveCompteur = new int[NBOXES];
    for(i = 0; i < NBOXES; i++)
    {
        boxesFileGet[i].ConnexionClosed();
        boxesMessageRetour[i].ConnexionClosed();
        boxesFileReceive[i].ConnexionClosed();
        fileGetCompteur[i] = 0;
        fileReceiveCompteur[i] = 0;
    };
    lockFileGetCompteur = new Lock("lock file get compteur");
    lockFileReceiveCompteur = new Lock("lock file receive compteur");
};
FileServer::~FileServer(){
    delete [] boxesFileGet;
    delete [] boxesMessageRetour;
    delete [] boxesFileReceive;
    delete fileGetCompteur;
    delete fileReceiveCompteur;
    delete lockFileReceiveCompteur;
    delete lockFileGetCompteur;
};



//----------------------------------------------------------------------------------------------------------
//
// Initialise le protocole FTP
//
//----------------------------------------------------------------------------------------------------------
extern Thread *currentThread;

OwnFTP::OwnFTP(PostOffice *office): Protocole(office)
{
    myPostOffice = office;
    tcpProtocole = new OwnTCP(office);
    //files = new FileServer[myPostOffice->getNumBox()];
}

OwnFTP::~OwnFTP()
{
    delete tcpProtocole;
}

void 
OwnFTP::CloseAllConnexion() 
{
    tcpProtocole->CloseAllConnexion();
}

bool 
OwnFTP::CloseConnexion(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data) 
{
    return tcpProtocole->CloseConnexion(pktHdr, mailHdr, data);
}

bool 
OwnFTP::OpenConnexion(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data) 
{
    return tcpProtocole->OpenConnexion(pktHdr, mailHdr, data);
}


void
OwnFTP::HandleMessage(MailHeader mailHdr, PacketHeader pktHdr, char *buffer)
{
    if(mailHdr.type == MailHeader::ACK) {
        //Réception d'un ACK
        myPostOffice->MarkACKReceip(mailHdr.from);
    } else if (mailHdr.type == MailHeader::ACKCONNEXION){
        //On etablie la connexion
        myPostOffice->getBoxes()[mailHdr.from].ConnexionOppened();
    } else if (mailHdr.type == MailHeader::CONNEXION){
        //On envoi un ACK pour dire que l'on peut se connecter
        SendACK(&pktHdr, &mailHdr);
    } else if (mailHdr.type == MailHeader::CLOSED){
        SendACK(&pktHdr, &mailHdr);
        switch(myPostOffice->getBoxes()[mailHdr.from].GetConnexion()){
            //cas de la machine réceptrice
            case MailBox::OPEN:
                myPostOffice->getBoxes()[mailHdr.from].SetConnexion(MailBox::WAITSENDEND);
                interrupt->Schedule(TimerWaitSendEnd, (int)mailHdr.from, (MAXREEMISSIONS * TEMPO * 2), TimerInt);
                break;
            case MailBox::CLOSE:
                myPostOffice->getBoxes()[mailHdr.from].SetConnexion(MailBox::WAITSENDEND);
                interrupt->Schedule(TimerWaitSendEnd, (int)mailHdr.from, (MAXREEMISSIONS * TEMPO * 2), TimerInt);
                break;

            //cas machine émettrice
            case MailBox::WAITCLOSE:
                myPostOffice->getBoxes()[mailHdr.from].SetConnexion(MailBox::WAIT);
                interrupt->Schedule(TimerWait, (int)mailHdr.from, (MAXREEMISSIONS * TEMPO * 2), TimerInt);
                break;
            case MailBox::WAITSENDEND:
                //Nothing to do 
                break;
            case MailBox::WAIT:
                //Nothing to do
                break;
            default:
                printf("\nClosed receive state not handle %d\n", myPostOffice->getBoxes()[mailHdr.from].GetConnexion());
                break;
        }
    } else if (mailHdr.type == MailHeader::MESSAGE){
        //Reception d'un message
        //Envoi du packet ACK
        SendACK(&pktHdr, &mailHdr);
        //Ajout du message au tampon
        myPostOffice->getBoxes()[mailHdr.from].AjouterMessage(pktHdr, mailHdr, buffer + sizeof(mailHdr), mailHdr.from);
    } else if (mailHdr.type == MailHeader::FILEGET)
    {
        tcpProtocole->SendACK(&pktHdr, &mailHdr);
        //On ajoute le message dans le tampon get du fileServer
        mailHdr.idPIDSender = mailHdr.idPID;
        bool iscomplete = fileServer->AjouterMessageFileGet(pktHdr, mailHdr, buffer + sizeof(mailHdr), mailHdr.from);
        //On lance la procédure pour envoyer un fichier
        if(iscomplete)
        {
            fileServer->IncrementeCompteurMessageFileGet(mailHdr.from);
            //si le message est complet on va pouvoir ouvrir le fichier et l'envoyer
            sendFile->V();
        }

    } else if (mailHdr.type == MailHeader::FILESEND)
    {
        tcpProtocole->SendACK(&pktHdr, &mailHdr);
        //On ajoute le message dans le tampon receive du fileServer
        fileServer->AjouterMessageFileReceive(pktHdr, mailHdr, buffer + sizeof(mailHdr), mailHdr.from);

    } else if (mailHdr.type == MailHeader::ENDFILE)
    {
        tcpProtocole->SendACK(&pktHdr, &mailHdr);
        //Un fichier complet est arrivé
        fileAvailable->V();
    } else if(mailHdr.type == MailHeader::ERROR)
    {
        if(mailHdr.idPIDSender != 0)
        {
            //On ajoute au message de retour 
            int n;
            sscanf(buffer, "%d", &n);
            fileServer->PutFileReturn(n, mailHdr.idPIDSender, mailHdr.from);
        }

    } else {
        printf("\ntype message non connu %d\n", mailHdr.type);
    }
}

/**
 * Envoi d'un fichier. Retourne 1 si il a bien été envoyé, un code d'erreur sinon
*/
int 
OwnFTP::SendFile(PacketHeader *pktHdr, MailHeader *mailHdr, const char *fileName)
{
    //On vérifie qu'on a réussi à ouvrir le fichier
    if(!fileServer->ServerOpenFile((char *)fileName))
    {
        //Erreur d'ouverture du fichier
        return ERR_OPEN_FILE;
    }
    int size = fileServer->SizeFile();
    char *buffer;
    buffer = new char[size];
    if(!fileServer->ServerReadFile(buffer))
    {
        //Erreur lecture fichier
        delete buffer;
        return ERR_READ_FILE;
    }
    mailHdr->length = strlen(fileName);
    if(!SplitSend(pktHdr, mailHdr, fileName, MailHeader::FILESEND))
    {
        //Erreur envoi nom fichier
        delete buffer;
        return ERR_SENDNAME_FILE;
    }
    //envoie le nombre de bits dans le fichier
    char taille[MAX_INT_SIZE + 1];
    snprintf(taille,MAX_INT_SIZE+1,"%d",size);
    mailHdr->length = strlen(taille);
    if(!SplitSend(pktHdr, mailHdr, taille, MailHeader::FILESEND))
    {
        //Erreur envoi taille du fichier
        delete buffer;
        return ERR_SENDSIZE_FILE;
    }
    mailHdr->length = strlen(buffer);
    if(!SplitSend(pktHdr, mailHdr, buffer, MailHeader::FILESEND))
    {
        //Erreur envoie contenu du fichier
        delete buffer;
        return ERR_SENDCONTENT_FILE;
    }
    //Envoi du message comme quoi on a finit 
    mailHdr->length = 1;
    if(!SplitSend(pktHdr, mailHdr, "", MailHeader::ENDFILE))
    {
        //Erreur envoie fin de fichier
        delete buffer;
        return ERR_SENDEND_FILE;
    }
    delete buffer;
    return 1;
}

/**
 * Demande de fichier à une machine distante. Retourne 1 si on a reçu le fichier,
 * un code d'erreur sinon.
*/
int 
OwnFTP::GetFile(PacketHeader *pktHdr, MailHeader *mailHdr, const char *fileName)
{
    //On veut récupérer un fichier, envoie demande
    unsigned pid = SplitSend(pktHdr, mailHdr, fileName, MailHeader::FILEGET);
    if(pid == 0)
    {
        //Pas réussi à envoyer le message 
        return ERR_SEND_MESSAGE;
    } else 
    {
        currentThread->Wait(MAXREEMISSIONS * TEMPO * 5);
        if(fileServer->IsEmptyReturn(mailHdr->to))
        {
            //Le message d'erreur a du se perdre
            return ERR_NO_ERROR;
        }
        int n = fileServer->GetFileReturn(pid, mailHdr->to);
        //Le message a été envoyé on se met en attente de la réception, avec un timer 
        return n;

    }
    return 1;
}

//----------------------------------------------------------------------------------------------------------------------
//
// Fonction utilisant TCP
//
//----------------------------------------------------------------------------------------------------------------------

int 
OwnFTP::SplitSend(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data, int type) 
{
    return tcpProtocole->SplitSend(pktHdr, mailHdr, data, type);
}
void 
OwnFTP::SendACK(PacketHeader *pktHdr, MailHeader *mailHdr)
{
    tcpProtocole->SendACK(pktHdr, mailHdr);
} 
bool 
OwnFTP::SecureSend(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data, int type)
{
    return tcpProtocole->SecureSend(pktHdr,mailHdr,data, type);
}
void 
OwnFTP::Receive(int box, PacketHeader *pktHdr, MailHeader *mailHdr, char* data)
{
    tcpProtocole->Receive(box,pktHdr,mailHdr,data);
}