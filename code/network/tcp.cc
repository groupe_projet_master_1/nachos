#include "tcp.h"
#include "post.h"
#include "interrupt.h"
#include "system.h"

//-----------------------------------------------------------------------------------------------------------------
//
// Timer
//
//-----------------------------------------------------------------------------------------------------------------

/**
 * Timer qui ferme la connexion si on reçoit pas de message END
 */
void 
TimerWaitClose(int n)
{
    if(postOffice->getBoxes()[n].GetConnexion() == MailBox::WAITCLOSE)
    {
        postOffice->getBoxes()[n].SetConnexion(MailBox::CLOSE);
    }
}

/**
 * Timer qui envoi des messages END au client et ferme la connexion
*/
void 
TimerWaitSendEnd(int n)
{
    if(postOffice->getBoxes()[n].GetConnexion() == MailBox::WAITSENDEND)
    {
        PacketHeader outPktHdr;
        MailHeader outMailHdr;
        outMailHdr.from = postOffice->getNetAddr();
        outMailHdr.length = 1;
        outPktHdr.to = n;		
        outMailHdr.to = n;
        postOffice->GetProtocole()->CloseConnexion(&outPktHdr, &outMailHdr, "");
        postOffice->getBoxes()[n].SetConnexion(MailBox::CLOSE);
    }
}

/**
 * Timer ferme la connexion si ACK non reçu lors de l'envoi de END
*/
void 
TimerWait(int n)
{
    if(postOffice->getBoxes()[n].GetConnexion() == MailBox::WAIT)
    {
        postOffice->getBoxes()[n].SetConnexion(MailBox::CLOSE);
    }
}

/**
 * Récupére une partie d'une chaine de caractère :
 *  data[debData] -> data[debData + size]
*/
void 
splitCharArray(const char *data, char *newArray, int debData, int size)
{
    int i;
    for(i = 0; i < size; i++)
    {
        newArray[i] = data[debData + i];
    }
}

//--------------------------------------------------------------------------------------------------------------
//
//Protocole TCP
//
//--------------------------------------------------------------------------------------------------------------
//thread courant
extern Thread *currentThread;

OwnTCP::OwnTCP(PostOffice *office): Protocole(office)
{
    myPostOffice = office;
    idMessage = 1;
}

OwnTCP::~OwnTCP()
{
}

/**
 * Envoie d'un message, découpé si il est trop volumineux
 * retourne true si tout le message a été envoyé et qu'on a reçu les ACK
 * retourne false si au moins une partie du message s'est perdue (ou qu'on a pas reçu l'ACK)
*/
int 
OwnTCP::SplitSend(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data, int type) 
{
    if(!(myPostOffice->getBoxes()[mailHdr->to].ConnexionIsOpen() || myPostOffice->getBoxes()[mailHdr->to].ConnexionIsClose()))
    {
        //La connexion est en cours de fermeture on annule l'envoie
        return 0;
    }
    unsigned size = mailHdr->length;
    unsigned current = 0;
    //Calcul du nombre de message à envoyer
    mailHdr->nbMessage = divRoundUp(size, MaxMailSize);
    mailHdr->idPID = idMessage;
    idMessage++;
    mailHdr->offSet = 0;
    while(size > MaxMailSize)
    {
        char *dataPart = new char[MaxMailSize];
        splitCharArray(data, dataPart, current, MaxMailSize);
        mailHdr->length = MaxMailSize;
        if(!SecureSend(pktHdr, mailHdr, dataPart, type))
        {
            //la partie du message n'a pas réussi à être envoyée on retourne une erreur à l'utilisateur
            delete dataPart;
            return 0;
        }
        current += MaxMailSize;
        size -= MaxMailSize;
        mailHdr->offSet++;   
        delete dataPart;
    }
    //Envoi du dernier message
    char *dataPart = new char[size];
    splitCharArray(data, dataPart, current, size);
    mailHdr->length = size;
    if(!SecureSend(pktHdr, mailHdr, dataPart, type)){
        //la partie du message n'a pas réussi à être envoyée
        delete dataPart;
        return 0;
    }
    delete dataPart;
    return (int)idMessage -1;
}

/*
    On ne vérifie pas que la connexion est ouverte pour envoyer un 
    ACK. On part du principe que si un message est reçu alors la connexion est
    ouverte avec la machine qui a envoyé le message.
    Envoie un ACK à la machine émettrice du message.
*/
void 
OwnTCP::SendACK(PacketHeader *pktHdr, MailHeader *mailHdr) 
{
    PacketHeader outPktHdr;
    MailHeader outMailHdr;
    // Creation de l'entête
    outPktHdr.to = pktHdr->from;
    outPktHdr.from = myPostOffice->getNetAddr();
    outPktHdr.length = sizeof(MailHeader) + 1;
    //Création du mailHdr
    outMailHdr.to = mailHdr->from;
    outMailHdr.from = mailHdr->to;
    outMailHdr.length = 1;
    outMailHdr.type = MailHeader::ACK;
    myPostOffice->Send(outPktHdr, outMailHdr, "");
    return;
}

/**
 * Envoie "fiable" d'un message. On vérifie que la connexion est ouverte (si non on l'initialise)
 * Puis on essaye d'envoyer le message jusqu'à réception d'un ACK ou MAXREEMISSIONS tentatives. 
 * Si on reçoit un ACK on retourne true sinon on retourne false
*/
bool 
OwnTCP::SecureSend(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data, int type) 
{
    int i;

    if(!myPostOffice->getBoxes()[mailHdr->to].ConnexionIsOpen()){
        //si la connexion n'est pas ouverte on l'initialise
        if(!OpenConnexion(pktHdr, mailHdr, data)){
            printf("\nEchec de l'ouverture de la connexion\n");
            return false;
        }
    }
    myPostOffice->ClearACKReceip(mailHdr->to);
    switch(type){
        case 0:
            mailHdr->type = MailHeader::ACK;
            break;
        case 1:
            mailHdr->type = MailHeader::MESSAGE;
            break;
        case 2:
            mailHdr->type = MailHeader::CONNEXION;
            break;
        case 3:
            mailHdr->type = MailHeader::CLOSED;
            break;
        case 4:
            mailHdr->type = MailHeader::FILEGET;
            break;
        case 5:
            mailHdr->type = MailHeader::FILESEND;
            break;
        case 6:
            mailHdr->type = MailHeader::ACKCONNEXION;
            break;
        case 7:
            mailHdr->type = MailHeader::ERROR;
            break;
        case 8:
            mailHdr->type = MailHeader::ENDFILE;
            break;
        case 9:
            mailHdr->type = MailHeader::FILESENDEND;
            break;
        default:
            mailHdr->type = MailHeader::MESSAGE;
            printf("\ntype unknow\n");
            break;
    }
    // Envoi du message
    for(i = 0; i < MAXREEMISSIONS; i++)
    {
        if(myPostOffice->getBoxes()[mailHdr->to].ConnexionIsOpen()){
            //Connexion ouverte
            myPostOffice->Send(*pktHdr, *mailHdr, data);
        } else {
            if (DebugIsEnabled('n'))
            {
                printf("Connexion closed: Ignoring sending");
            }
            myPostOffice->MarkACKReceip(mailHdr->to);
            return false;
        }
        currentThread->Wait(TEMPO);

        if(myPostOffice->TestACKReceip(mailHdr->to)){
            break;
        }
    }
    //printf("ACK MESSAGE (try=%d) : %s\n", i, (i < MAXREEMISSIONS) ? "Done" : "Fail");
    return (i < MAXREEMISSIONS);
}

/**
 * Réception d'un message dans la boîte aux lettres numéro box.
 * Si le message est découpé, il est assemblé et retourné à l'utilisateur.
 * (Fonction en attente tant qu'il n'y a pas de message dans la box).
 * Précondition : 
 *  - L'utilisateur doit donner un buffer de taille suffisante pour réceptionner le message. 
*/
void 
OwnTCP::Receive(int box, PacketHeader *pktHdr, MailHeader *mailHdr, char* data) 
{
    int nbMessageTotal;
    char *tmp;
    tmp = new char[MaxMailSize];
    myPostOffice->getBoxes()[box].Get(pktHdr, mailHdr, tmp);
    ASSERT(mailHdr->length <= MaxMailSize);
    nbMessageTotal = mailHdr->nbMessage;
    //On ajoute le premier morceau du message
    memcpy(data, tmp, mailHdr->length);
    delete tmp;
    int i;
    //Ajout des autres morceaux si il y en a 
    for(i = 1; i < nbMessageTotal; i++)
    {
        tmp = new char[MaxMailSize];
        myPostOffice->getBoxes()[box].Get(pktHdr, mailHdr, tmp);
        ASSERT(mailHdr->length <= MaxMailSize);
        memcpy(data + i * MaxMailSize, tmp, mailHdr->length);
        delete tmp;
    }
}

/**
 * Fermeture de toutes les connexions
*/
void 
OwnTCP::CloseAllConnexion() 
{
    int i;
    PacketHeader outPktHdr;
    MailHeader outMailHdr;
    outMailHdr.from = myPostOffice->getNetAddr();
    outMailHdr.length = 1;
    for(i = 0; i < myPostOffice->getNumBox(); i++)
    {
        //pour chaque boîte qui a une connexion ouverte on envoi un message de fermeture
        if(myPostOffice->getBoxes()[i].ConnexionIsOpen())
        {
            outPktHdr.to = i;		
            outMailHdr.to = i;
            CloseConnexion(&outPktHdr, &outMailHdr, "");
            //fermeture de connexion
            myPostOffice->getBoxes()[i].SetConnexion(MailBox::WAITCLOSE);
            //lancer un timer Wait Close
            interrupt->Schedule(TimerWaitClose, (int)i, (MAXREEMISSIONS * TEMPO * 5), TimerInt);
        }
        //tant que la connexion n'est pas fermée on attend
        while(!myPostOffice->getBoxes()[i].ConnexionIsClose())
        {
            currentThread->Wait((TEMPO));
        }
    }
}

/**
 * Demande de fermeture de connexion.
 * On essaye d'envoyer le message END jusqu'à réception d'un ACK ou MAXREEMISSIONS tentatives. 
 * Si on reçoit un ACK on retourne true sinon on retourne false
*/
bool 
OwnTCP::CloseConnexion(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data) 
{
    int i;
    mailHdr->type = MailHeader::CLOSED;
    myPostOffice->ClearACKReceip(mailHdr->to);
    //  Envoie des messages
    for(i = 0; i < MAXREEMISSIONS && !myPostOffice->getBoxes()[mailHdr->to].ConnexionIsClose(); i++)
    {
        myPostOffice->Send(*pktHdr, *mailHdr, data);
        currentThread->Wait(TEMPO);
        if(myPostOffice->TestACKReceip(mailHdr->to)){
            break;
        }
    }
    //printf("ACK CLOSE (try=%d) : %s\n", i, (i < MAXREEMISSIONS) ? "Done" : "Fail");
    return (i < MAXREEMISSIONS);
}

/**
 * Demande d'ouverture de connexion.
 * On essaye d'envoyer le message CONNEXION jusqu'à réception d'un ACK ou MAXREEMISSIONS tentatives. 
 * Si on reçoit un ACK on retourne true sinon on retourne false
*/
bool 
OwnTCP::OpenConnexion(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data) 
{
    int i;
    mailHdr->type = MailHeader::CONNEXION;
    myPostOffice->ClearACKReceip(mailHdr->to);
    for(i = 0; i < MAXREEMISSIONS && !myPostOffice->getBoxes()[mailHdr->to].ConnexionIsOpen(); i++)
    {
        myPostOffice->Send(*pktHdr, *mailHdr, data);
        currentThread->Wait(TEMPO);
        if(myPostOffice->TestACKReceip(mailHdr->to)){
            //La connexion est prete à être ouverte
            SendACKConnexion(pktHdr, mailHdr);
            myPostOffice->getBoxes()[mailHdr->to].ConnexionOppened();
            break;
        }
    }
    //printf("ACK OPEN (try=%d) : %s\n", i, (i < MAXREEMISSIONS) ? "Done" : "Fail");
    return (i < MAXREEMISSIONS);
}

/**
 * Comme le ACK on ne vérifie pas que la conenxion est ouverte.
 * Envoie un ACKCONNEXION pour dire à la machine distante qu'on est bien connecté.
*/
void 
OwnTCP::SendACKConnexion(PacketHeader *pktHdr, MailHeader *mailHdr) 
{
    PacketHeader outPktHdr;
    MailHeader outMailHdr;
    outPktHdr.to = pktHdr->to;
    outPktHdr.from = myPostOffice->getNetAddr();
    outPktHdr.length = sizeof(MailHeader) + 1;
    outMailHdr.to = mailHdr->to;
    outMailHdr.from = mailHdr->from;
    outMailHdr.length = 1;
    outMailHdr.type = MailHeader::ACKCONNEXION;
    myPostOffice->Send(outPktHdr, outMailHdr, "");
    return;
}

/**
 * Gestion des types de message reçu.
*/
void
OwnTCP::HandleMessage(MailHeader mailHdr, PacketHeader pktHdr, char *buffer) 
{
    if(mailHdr.type == MailHeader::ACK) {
        //Réception d'un ACK
        myPostOffice->MarkACKReceip(mailHdr.from);
    } else if (mailHdr.type == MailHeader::ACKCONNEXION){
        //On etablie la connexion
        myPostOffice->getBoxes()[mailHdr.from].ConnexionOppened();
    } else if (mailHdr.type == MailHeader::CONNEXION){
        //On envoi un ACK pour dire que l'on peut se connecter
        SendACK(&pktHdr, &mailHdr);
    } else if (mailHdr.type == MailHeader::CLOSED){
        SendACK(&pktHdr, &mailHdr);
        switch(myPostOffice->getBoxes()[mailHdr.from].GetConnexion()){
            //cas de la machine réceptrice
            case MailBox::OPEN:
                myPostOffice->getBoxes()[mailHdr.from].SetConnexion(MailBox::WAITSENDEND);
                interrupt->Schedule(TimerWaitSendEnd, (int)mailHdr.from, (MAXREEMISSIONS * TEMPO * 2), TimerInt);
                break;
            case MailBox::CLOSE:
                myPostOffice->getBoxes()[mailHdr.from].SetConnexion(MailBox::WAITSENDEND);
                interrupt->Schedule(TimerWaitSendEnd, (int)mailHdr.from, (MAXREEMISSIONS * TEMPO * 2), TimerInt);
                break;

            //cas machine émettrice
            case MailBox::WAITCLOSE:
                myPostOffice->getBoxes()[mailHdr.from].SetConnexion(MailBox::WAIT);
                interrupt->Schedule(TimerWait, (int)mailHdr.from, (MAXREEMISSIONS * TEMPO * 2), TimerInt);
                break;
            case MailBox::WAITSENDEND:
                //Nothing to do 
                break;
            case MailBox::WAIT:
                //Nothing to do
                break;
            default:
                printf("\nClosed receive state not handle %d\n", myPostOffice->getBoxes()[mailHdr.from].GetConnexion());
                break;
        }
    } else if (mailHdr.type == MailHeader::MESSAGE){
        //Reception d'un message
        //Envoi du packet ACK
        SendACK(&pktHdr, &mailHdr);
        //Ajout du message au tampon
        myPostOffice->getBoxes()[mailHdr.from].AjouterMessage(pktHdr, mailHdr, buffer + sizeof(mailHdr), mailHdr.from);
    } else {
        printf("\ntype message non connu %d\n", mailHdr.type);
    }
}

/**
 * Inutilisable dans tcp. Renvoie un code d'erreur
*/
int 
OwnTCP::SendFile(PacketHeader *pktHdr, MailHeader *mailHdr, const char *fileName)
{
    return ERR_PROTOCOLE;
}

/**
 * Inutilisable dans tcp. Renvoie un code d'erreur
*/
int 
OwnTCP::GetFile(PacketHeader *pktHdr, MailHeader *mailHdr, const char *fileName)
{
    return ERR_PROTOCOLE;
}