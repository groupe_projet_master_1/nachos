#ifndef __MAILTAMPON_H__
#define __MAILTAMPON_H__

#include "thread.h"
#include "synchlist.h"
#include "synch.h"
class Mail;
class PacketHeader;
class MailHeader;

//Classe contenant les informations nécessaires à l'exécution des timer de contrôle.
class OwnTimer {
    public:
        OwnTimer(int numMessage, int box, unsigned off){
            currentMessage = numMessage;
            numBox = box;
            offset = off;
        }
        unsigned currentMessage;
        int numBox;
        unsigned offset;
};

/**
 * Classe contenant le message courant avant qu'il soit ajouté dans une boîte aux lettres
 * Le tampon contient seulement des messages avec le même id. 
 * Soit 1 message, soit tous les morceaux d'un gros message.
*/
class MailTampon {
    public:
        MailTampon();
        ~MailTampon();
        void Acquire(); //vérrouille le lock
        void Release(); //relâche le lock
        void AjouterDonnee(PacketHeader pktHdr, MailHeader mailHdr, char *data); //ajoute un message dans le tampon (à la fin)
        Mail *GetMail(); //retire le premier message de la liste
        bool IsMessage(); //vrai si le tampon contient un message
        unsigned IdMail(); //renvoi l'ID du dernier message du tampon
        unsigned OffsetMail(); //renvoi l'offset du dernier message du tampon
        bool IsMessageComplete(); //vrai si le message dans le tampon est complet
    private:
        Lock *lock;
        SynchList *messages;
};

#endif