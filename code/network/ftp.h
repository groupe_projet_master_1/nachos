#ifndef __FTP_H__
#define __FTP_H__
#include "bitmap.h"
#include "protocole.h"
#include "synchlist.h"
#include "mailtampon.h"
//-----------------------------------------------------------------------------------
//
// Code d'erreur
//
//-----------------------------------------------------------------------------------

#define ERR_PROTOCOLE           -1
#define ERR_OPEN_FILE           -2
#define ERR_READ_FILE           -3
#define ERR_SENDNAME_FILE       -4
#define ERR_SENDSIZE_FILE       -5
#define ERR_SENDCONTENT_FILE    -6
#define ERR_SENDEND_FILE        -7
#define ERR_SEND_MESSAGE        -8
#define ERR_CREATE_FILE         -9
#define ERR_WRITE_FILE          -10
#define ERR_NO_ERROR            -11

/**
 * Sert à stocker les messages d'erreur et les codes de retour lorsqu'on 
 * demande un fichier à une machine distante.
*/
class MailRetour
{
    public:
        MailRetour(int returnCode, unsigned idmail)
        {
                retCode = returnCode;
                idMessage = idmail;
        }
        
        int retCode; //Code de retour, 1 si tout va bien sinon un des codes d'erreur décrit au dessus
        unsigned idMessage ; //Id du message ayant demander l'envoi du fichier
};

class MailBox;

//-----------------------------------------------------------------------------------
//
// Réception fichier
//
//-----------------------------------------------------------------------------------
class FileServer
{
    public:
        static FileServer& Instance(void);
        
        void CreateFile();
        bool ServerOpenFile(char *name);
        bool ServerReadFile(char *buffer);
        void Resize(size_t newSize, char *buffer, size_t lastSize);
        int SizeFile();
        void SendFile();
//-----------------------------------------------------------------------------------
// Reception box function file receive
//-----------------------------------------------------------------------------------
        void PutFileReceive(PacketHeader pktHdr, MailHeader mailHdr, char *data, int numBox);
        void GetFileReceive(PacketHeader *pktHdr, MailHeader *mailHdr, char *data, int numBox);
        void ReceiveFileReceive(PacketHeader *pktHdr, MailHeader *mailHdr, char* data, int numBox);
        //bool IsEmptyFileReceive(); 
        void TimerEnleverMessageFileReceive(unsigned currentMessage, unsigned offset, int numBox);
        void AjouterMessageFileReceive(PacketHeader pktHdr, MailHeader mailHdr, char *data, int numBox); 
        void IncrementeCompteurMessageFileReceive(int numBox);
        bool FileCompleteReceiveCompteur(int numBox)
        {
                lockFileReceiveCompteur->Acquire();
                bool test = fileReceiveCompteur[numBox] >= 2;
                lockFileReceiveCompteur->Release();
                return test;
        }
        void Find3MessagesFileReceive(PacketHeader *inPktHdr1, MailHeader *inMailHdr1, char *buffer1, PacketHeader *inPktHdr2, MailHeader *inMailHdr2, char *buffer2, PacketHeader *inPktHdr3, MailHeader *inMailHdr3, char *buffer3, int numBox);
        void DecrementeCompteurMessageFileReceive(int numBox);
        //void ReceiveFileReceive(PacketHeader *pktHdr, MailHeader *mailHdr, char* data) ;
//-----------------------------------------------------------------------------------
// Reception box function file Send
//-----------------------------------------------------------------------------------
        void PutFileGet(PacketHeader pktHdr, MailHeader mailHdr, char *data, int numBox);
        void GetFileGet(PacketHeader *pktHdr, MailHeader *mailHdr, char *data, int numBox); 
        //bool IsEmptyFileGet(); 
        void TimerEnleverMessageFileGet(unsigned currentMessage, unsigned offset, int numBox);
        bool AjouterMessageFileGet(PacketHeader pktHdr, MailHeader mailHdr, char *data, int numBox); 
        void IncrementeCompteurMessageFileGet(int numBox);
        void ReceiveFileGet(PacketHeader *pktHdr, MailHeader *mailHdr, char* data, int numBox) ;
        void DecrementeCompteurMessageFileGet(int numBox);
        bool FileCompleteGetCompteur(int numBox)
        {
                bool test = false;
                lockFileGetCompteur->Acquire();
                test = fileGetCompteur[numBox] > 0;
                lockFileGetCompteur->Release();
                return test;
        }
//-----------------------------------------------------------------------------------
// Fonction retour sur reception de fichier
//-----------------------------------------------------------------------------------
        void PutFileReturn(int returnCode, unsigned idMail, int numBox);
        int GetFileReturn(unsigned idMail, int numBox); 
        bool IsEmptyReturn(int numBox);
    private:
        FileServer();
        ~FileServer();
        static FileServer m_instance;
        OpenFile *file;
//-----------------------------------------------------------------------------------
// Reception box file Send
//-----------------------------------------------------------------------------------
        MailBox *boxesFileGet;
        int *fileGetCompteur;
        Lock *lockFileGetCompteur;
        /*SynchList *messagesFileGet;
        MailTampon *tamponFileGet;*/

//-----------------------------------------------------------------------------------
// Reception box file receive
//-----------------------------------------------------------------------------------
        MailBox *boxesFileReceive;
        int *fileReceiveCompteur;
        Lock *lockFileReceiveCompteur;
        /*SynchList *messagesFileReceive;
        MailTampon *tamponFileReceive;*/
//-----------------------------------------------------------------------------------
// Retour sur reception de fichier
//-----------------------------------------------------------------------------------
        MailBox *boxesMessageRetour;
        /*SynchList *messageRetour;*/
};

class PostOffice;
class PacketHeader;
class MailHeader;
class OwnTCP;

class OwnFTP: public Protocole {
    public:
        OwnFTP(PostOffice *office);
        ~OwnFTP();
        void HandleMessage(MailHeader mailHdr, PacketHeader pktHdr, char *buffer);
        bool CloseConnexion(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data);
        bool OpenConnexion(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data);
        void CloseAllConnexion();
        int SendFile(PacketHeader *pktHdr, MailHeader *mailHdr, const char *fileName);
        void SendACK(PacketHeader *pktHdr, MailHeader *mailHdr); //fonction pour envoyer un acquittement 
        bool SecureSend(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data, int type);
        int SplitSend(PacketHeader *pktHdr, MailHeader *mailHdr, const char *data, int type) ;
        void Receive(int box, PacketHeader *pktHdr, MailHeader *mailHdr, char* data) ;
        int GetFile(PacketHeader *pktHdr, MailHeader *mailHdr, const char *fileName);
    private:
        OwnTCP *tcpProtocole;
        //FileServer *files;
};

#endif