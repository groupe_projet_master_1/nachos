#include "copyright.h"
#include "system.h"
#include "synchconsole.h"
#include "synch.h"

//Semaphores utiles pour rendre la console bloquante
static Semaphore *readAvail;
static Semaphore *writeDone;

//Semaphores utiles au multi-threading
static Semaphore *currentRead;
static Semaphore *currentWrite;
Lock *longString;

static void ReadAvail(int arg) {
  readAvail->V();
}

static void WriteDone(int arg) {
  writeDone->V();
}

//-------------------------
// SynchConsole :
//    Console synchronisée 
//-------------------------
SynchConsole::SynchConsole(char *readFile, char *writeFile) {
  readAvail = new Semaphore("read avail", 0);
  writeDone = new Semaphore("write done", 0);
  currentRead = new Semaphore("current read", 1);
  currentWrite = new Semaphore("current write", 1);
  console = new Console (readFile, writeFile, ReadAvail, WriteDone, 0);
  longString = new Lock("locklong");
}

SynchConsole::~SynchConsole() {
  delete console;
  delete writeDone;
  delete readAvail;
  delete currentRead;
  delete currentWrite;
}

/*
 * Affiche un charactere de manière synchrone
 */
void SynchConsole::SynchPutChar(const char ch) {
    currentWrite->P();
	  console->PutChar(ch);	// echo it!
	  writeDone->P();      	// wait for write to finish
    currentWrite->V();
}

/*
 * Recupere un charactere de manière synchrone
 */
char SynchConsole::SynchGetChar() {
    currentRead->P();
	  readAvail->P();      	// wait for character to arrive
    char c = console->GetChar();

    currentRead->V();
	  return c;
}

/*
 * Affiche une chaine de charactere de manière synchrone
 */
void SynchConsole::SynchPutString(const char s[]) {
  int i = 0;
  currentWrite->P();
  while (s[i] != '\0') {
	  console->PutChar(s[i]);	// echo it!
	  writeDone->P();      	  // wait for write to finish
    i++;
  }
  currentWrite->V();
}

/*
 * Recupere une chaine de charactere de manière synchrone
 */
void SynchConsole::SynchGetString(char *s, int n) {
  int i = 0;
  currentRead->P();
  
  //On récupère la chaine de taille n
  for(; i < n; i++) {
	  readAvail->P();
	  s[i] = console->GetChar();
  }
  s[i] = '\0';

  currentRead->V();
}

/*
 * Affiche un entier de manière synchrone
 */
void SynchConsole::SynchPutInt(int n) {
    int x;
    int size = MAX_INT_SIZE + 1;
    //Si le nombre est négatif, il faut la place pour le '-'
    if (n < 0) 
      size++;
    
    char chaine[size];
    //Impossible d'afficher un entier hors des bornes d'un entier
    if (MIN_INT <= n && n <= MAX_INT) {
      if((x = snprintf(chaine, size, "%d", n)) > 0 && x < size) {
        SynchPutString(chaine);
      } else{
        fprintf(stderr,"SynchPutInt: Something went wron with snprintf / code: %d", x);
      }
    } else {
      fprintf(stderr, "SynchPutInt: n should be between MIN_INT and MAX_INT");
    }
}

/*
 * Recupere un entier de manière synchrone
 */
int SynchConsole::SynchGetInt(int *n) {
    char chaine[MAX_INT_SIZE + 1];
    char c;
    int i, e;
    int positive = 1;
    currentRead->P();
    readAvail->P();
    c = console->GetChar();

    //On a un nombre négatif
    if (c == '-') {
      positive = -1;
      readAvail->P();
      c = console->GetChar();
    }

    for(i = 0; i < 10 && c >= '0' && c <= '9'; i++){
      chaine[i] = c;
      readAvail->P();
      c = console->GetChar();
    }
    currentRead->V();
    chaine[i] = '\0';
    
    //Si le chiffre est hors des bornes de int
    //sscanf va le limiter aux bornes
    if((e = sscanf(chaine, "%d", n)) != 1) {
      fprintf(stderr,"SynchGetInt: Something went wrong with sscanf / code: %d",e);
      return 1;
    }

    (*n) = (*n) * positive;
    return 0;
}

void SynchConsole::lockLong(){
  longString->Acquire();
}

void SynchConsole::unlockLong(){
  longString->Release();
}
