#include "userprog.h"
#include "synch.h"
#include "addrspace.h"
#include "machine.h"
#include "system.h"
#include "processusGroup.h"



void forkExecFunc(int arg){
    currentThread->space->InitRegisters ();	// set the initial register values
    currentThread->space->RestoreState ();	// load page table register

    initializeThread();
    currentThread->space->getThreads()->lock();
    int id = currentThread->space->getThreads()->ajoutThread();
    currentThread->space->getThreads()->setThread(id,currentThread);
    (&ProcessusGroup::Instance())->setThreadGroup(currentThread->space->getPid(),currentThread->space->getThreads());
    currentThread->space->getThreads()->unlock();
    if(id == -1){
        // erreur impossible
        fprintf(stderr,"ajout thread initial processus echoue\n");
        ASSERT(FALSE);
    }
    currentThread->idThread = id;

    machine->Run ();
}

int do_ForkExec(char *s){
    lockForkExec->Acquire();
    int fidExecutable = fileSystem->Open (s);

    if (fidExecutable == -1){
        fprintf (stderr,"ForkExec : Unable to open file %s\n", s);
        lockForkExec->Release();
        return -1;
    }
    OpenFile *exe = fileSystem->GetOpenFile(fidExecutable);
    int num = getNumPages(exe);
    if(!((&ProcessusGroup::Instance())->canInstanceProcess(num))) {
        fileSystem->Close(fidExecutable);
        fprintf (stderr,"ForkExec : no more space for this process %s\n", s);
        lockForkExec->Release();
        return -1;
    }
    AddrSpace *addrSpace = new AddrSpace (exe,num);
    Thread *t = new Thread(s);
    t->space = addrSpace;
    addrSpace->setSem(new UserSem());
    //ajout d'un processus dans le pool
    int pid = (&ProcessusGroup::Instance())->addProcessus(currentThread->space->getPid());
    t->space->setPid(pid);
    

    fileSystem->Close(fidExecutable);

    lockForkExec->Release();
    t->ForkExec(forkExecFunc, 0);
    return pid;
}

void do_ProcJoin(int pid){
    (&ProcessusGroup::Instance())->join(pid);
}