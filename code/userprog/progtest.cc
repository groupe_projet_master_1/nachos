// progtest.cc 
//      Test routines for demonstrating that Nachos can load
//      a user program and execute it.  
//
//      Also, routines for testing the Console hardware device.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "console.h"
#include "addrspace.h"
#include "synch.h"
#include "synchconsole.h"
#include "threadGroup.h"
#include "syscall.h"

//----------------------------------------------------------------------
// StartProcess
//      Run a user program.  Open the executable, load it into
//      memory, and jump to it.
//----------------------------------------------------------------------

void
StartProcess (char *filename)
{
    int fidExecutable = fileSystem->Open (filename);
    AddrSpace *space;

    if (fidExecutable == -1){
	  printf ("Unable to open file %s\n", filename);
	  return;
    }
    OpenFile *exe = fileSystem->GetOpenFile(fidExecutable);
    int num = getNumPages(exe);
    if(!((&ProcessusGroup::Instance())->canInstanceProcess(num))) {
        fileSystem->Close(fidExecutable);
        return;
    }
    space = new AddrSpace (exe,num);
    int pid = (&ProcessusGroup::Instance())->addProcessus(DIEU);
    currentThread->space = space;
    space->setPid(pid);

    initializeThread();
    (&ProcessusGroup::Instance())->setThreadGroup(pid,currentThread->space->getThreads());
    currentThread->space->getThreads()->lock();
    int id = currentThread->space->getThreads()->ajoutThread();

    

    if(id == -1){
        fprintf(stderr,"ajout thread initiale non fini\n");
        ASSERT(FALSE);
    }
    currentThread->space->getThreads()->setThread(id,currentThread);
    currentThread->idThread = id;
    currentThread->space->getThreads()->unlock();
    space->setSem(new UserSem());

    fileSystem->Close(fidExecutable);		// close file

    space->InitRegisters ();	// set the initial register values
    space->RestoreState ();	// load page table register

    machine->Run ();		// jump to the user progam
    ASSERT (FALSE);		// machine->Run never returns;
    // the address space exits
    // by doing the syscall "exit"
}

// Data structures needed for the console test.  Threads making
// I/O requests wait on a Semaphore to delay until the I/O completes.

static Console *console;
static Semaphore *readAvail;
static Semaphore *writeDone;

//----------------------------------------------------------------------
// ConsoleInterruptHandlers
//      Wake up the thread that requested the I/O.
//----------------------------------------------------------------------

static void ReadAvail(int arg) {
   readAvail->V();
}

static void WriteDone(int arg) {
    writeDone->V();
}

//----------------------------------------------------------------------
// ConsoleTest
//      Test the console by echoing characters typed at the input onto
//      the output.  Stop when the user types a 'q'.
//----------------------------------------------------------------------

void ConsoleTest(char *in, char *out) {
    char ch;

    readAvail = new Semaphore ("read avail", 0);
    writeDone = new Semaphore ("write done", 0);
    console = new Console (in, out, ReadAvail, WriteDone, 0);

    while (1) {
        readAvail->P();      	        // wait for character to arrive
        ch = console->GetChar();
        if (ch == EOF) {
            return;
        }
        console->PutChar(ch);	        // echo it!
        writeDone->P();      	        // wait for write to finish
    }
}

void SynchConsoleTest(char *in, char *out) {
    char ch;
    synchconsole = new SynchConsole(in, out);
    ch = synchconsole->SynchGetChar();
    while (ch != EOF) {
        synchconsole->SynchPutChar(ch);
        ch = synchconsole->SynchGetChar();
    }
    fprintf(stderr, "Solaris: EOF detected in SynchConsole!\n");
}