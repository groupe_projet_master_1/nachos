#ifndef FORKEXEC_H
#define FORKEXEC_H
#include "synch.h"
static Lock *lockForkExec = new Lock("lock fork exec");

int do_ForkExec(char *s);

#endif
