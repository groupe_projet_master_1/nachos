#include "userSem.h"

#include "processusGroup.h"
#include "system.h"
#include "bitmap.h"
#include "synch.h"

UserSem::UserSem() {
	nbSemMax = 8;
	group = new BitMap(nbSemMax);
	lock_Sem = new Lock("lock sem class");
	SemGroup = new Semaphore[nbSemMax];
	nbSem = 0;
}

UserSem::~UserSem() {
	delete lock_Sem;
	delete [] SemGroup;
	delete group;
}

/**
 * Redimensionne la taille du groupe de sémaphore
 * Retourne 0 si on ne peut pas aggrandir le groupe
 * Retourne 1 sinon
 */
int UserSem::redimension() {
	if (nbSemMax == MAX_NB_SEM) {
		return 0;
	}
	int augment;
	if (MAX_NB_SEM - nbSemMax > (int)sizeof(int)) {
		augment = (int)sizeof(int) - nbSemMax % sizeof(int);
	}
	else {
		augment = MAX_NB_SEM - nbSemMax;
	}
	BitMap *group_temp = new BitMap(nbSemMax + augment);
	Semaphore *SemGroup_temp = new Semaphore[nbSemMax + augment];
	int i;
	for(i = 0 ; i < nbSemMax ; i ++){
		if(group->Test(i)){
			SemGroup_temp[i] = SemGroup[i];
			group_temp->Mark(i);
		}
	}
	delete group;
	delete [] SemGroup;

	nbSemMax += augment;
	group = group_temp;
	SemGroup = SemGroup_temp;
	return 1;
}

int UserSem::addSem(int initValue) {
	lock_Sem->Acquire();
	if(nbSem + 1 >= nbSemMax){
		if(!redimension()){
			return SEM_FAIL;
		}
	}
	int indice = group->Find();
	SemGroup[indice].setValue(initValue);
	nbSem++;
	lock_Sem->Release();
	return indice;
}

void UserSem::removeSem(int indice){
	lock_Sem->Acquire();
	if(!group->Test(indice)) {
		fprintf(stderr, "Bad index semaphore: clear\n");
		(&ProcessusGroup::Instance())->KillProg(currentThread->space->getPid());
	}
	group->Clear(indice);
	nbSem--;
	lock_Sem->Release();
}

void UserSem::P(int indice){
	if(!group->Test(indice)) {
		fprintf(stderr, "Bad index semaphore : P\n");
		(&ProcessusGroup::Instance())->KillProg(currentThread->space->getPid());
	}
	SemGroup[indice].P();
}

void UserSem::V(int indice){
	if(!group->Test(indice)) {
		fprintf(stderr, "Bad index semaphore : V\n");
		(&ProcessusGroup::Instance())->KillProg(currentThread->space->getPid());
	}
	SemGroup[indice].V();
}

int do_initSem(int initValue){
	return currentThread->space->getSem()->addSem(initValue);
}

void do_P(int indice){
	currentThread->space->getSem()->P(indice);
}

void do_V(int indice){
	currentThread->space->getSem()->V(indice);
}

void do_clearSem(int indice){
	currentThread->space->getSem()->removeSem(indice);
}
