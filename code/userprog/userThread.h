#ifndef __USER_THREAD_H__
#define __USER_THREAD_H__

#include "thread.h"

class Condition;
class BitMap;
class Thread;

#define THREAD_SIZE (3 * PageSize)

#define MAX_OPEN_FILES_THREAD 10

class UserThread {
	private:
		int id;
		int num;
		int ret;
		int end;
		Condition *cond;
		int files[MAX_OPEN_FILES_THREAD];
		BitMap *bitFiles;
		int nbFile;
		Thread *thread;
	public:
		UserThread();
		~UserThread();
		int addFile(int fid);
		int getFile(int index);
		int removeFile(int index);
		void clearBitMap();

		//Setters
		void setId(int i){
			id = i;
		};
		void setCond(Condition *c){
			cond = c;
		};
		void setNum(int n){
			num = n;
		};
		void setEnd(int e){
			end = e;
		};
		void setRet(int r){
			ret = r;
		};

		//Getters
		int getId(){
			return id;
		};
		int getRet(){
			return ret;
		};
		int getEnd(){
			return end;
		};
		Condition *getCond(){
			return cond;
		};
		int getNum(){
			return num;
		};
		int getNbFile(){
			return nbFile;
		}
		void setThread(Thread *t){
			thread = t;
		}
		Thread *getThread(){
			return thread;
		}
		void killSelf();
};

#endif // __USER_THREAD_H__