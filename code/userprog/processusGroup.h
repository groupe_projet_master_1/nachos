#ifndef __PROCESSUS_GROUP_H__
#define __PROCESSUS_GROUP_H__
#include "frameprovider.h"
#include "addrspace.h"
#include "userProcessus.h"

#define MAX_PROCESSUS 8

class Lock;
class Condition;

extern void do_ProcJoin(int pid);

/**
 * Classe de pool de processus
 */
class ProcessusGroup {
    private:
        int nb;
        int nbProcessus;
        int nbProcessusMax;
        Lock *lock_Processus;
        Condition *cond_Processus;
        UserProcessus *pool;
        BitMap *bit;
        static ProcessusGroup m_instance;
        ProcessusGroup();
        ~ProcessusGroup();
        int indiceFromPid(int pid);

    public:
        static ProcessusGroup &Instance(void);
        void redimension();
        bool canInstanceProcess(int nbPages);
        int addProcessus(int ppid);
        void processusRemove(AddrSpace *space);
        bool noProcessusLeft();
        void waitCond();
        void broadcastCond();
        void deleteProcessusGroup();
        void join(int pid);
        void setThreadGroup(int pid,ThreadGroup *t);
        void KillProg(int pid);
	
};

#endif // __PROCESSUS_GROUP_H__