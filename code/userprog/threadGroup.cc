#include "threadGroup.h"

#include "bitmap.h"
#include "synch.h"
#include "machine.h"
#include "system.h"
#include "processusGroup.h"
#include "userprog.h"

ThreadGroup::ThreadGroup(int nb) {
	nbThreadsRunningMax = nb;
	taille = nb;
	nbThreadsRunning = 0;
	nbThreadsTotal = 0;
	idGen = 0;

	ensembleThread = new BitMap(nb);
	stackPlace = new BitMap(nbThreadsRunningMax);
	lock_Thread = new Lock("\nall user thread lock");

	userThreads = new UserThread[nb];
}

ThreadGroup::~ThreadGroup() {
	
	// elimination des conditions d'attente des threads restantes
	int i;
	for(i = 0 ; i < taille ; i++){
		if(ensembleThread->Test(i)){
			free(i);
		}
	}

	delete lock_Thread;
	delete ensembleThread;
	delete stackPlace;

	delete [] userThreads;
}

////////////////////////////////////////////////////////////////////////////////////
//								PRIVATE											  //

/**
 *	permet a l'ensemble thread de grandir si la memoire de certains thread n'est pas recupere
 *	renvoi 0 si la memoire a atteint sa taille maximale autorise
 */
int ThreadGroup::redimension(){
	if(nbThreadsTotal + 1 >= taille){
		if(taille == TAILLE_MAX_ENSEMBLE_THREAD){
			return 0;
		}
		int augment;
		if(TAILLE_MAX_ENSEMBLE_THREAD - taille > (int)sizeof(int)){
			augment = sizeof(int) - taille % sizeof(int);
		}
		else{
			augment = TAILLE_MAX_ENSEMBLE_THREAD - taille;
		}
		BitMap * ensembleThread_temp = new BitMap(taille + augment);
		UserThread *userThreads_temp = new UserThread[taille + augment];
		int i;
		for(i = 0 ; i < taille ; i++){
			if(ensembleThread->Test(i)){
				ensembleThread_temp->Mark(i);
				userThreads_temp[i] = userThreads[i];
			}
		}
		delete [] userThreads;
		delete ensembleThread;

		taille += augment;
		ensembleThread = ensembleThread_temp;
		userThreads = userThreads_temp;
		return 1;
	}
	return 1;
}

/**
 * renvoi l'index d'un thread dans le tableau representant les threads a partir de son id
 */
int ThreadGroup::find(int id) {
	int i;
	for(i = 0 ; i < taille ; i++){
		if(ensembleThread->Test(i) && userThreads[i].getId() == id){
			return i;
		}
	}
	return -1;
}

/**
 * exprime l'ajout d'un thread dans la structure
 */
void ThreadGroup::add() {
	nbThreadsRunning++;
	nbThreadsTotal++;
	idGen++;
}

/**
 * exprime le retrait d'un thread dans la structure(memoire recupere pour d'autre)
 */
void ThreadGroup::clear(int index) {
	nbThreadsTotal--;
	ensembleThread->Clear(index);
}

/**
 * libere la place du thread a l'index donne
 */
void ThreadGroup::free(int index){
	if(userThreads[index].getEnd() != 1){
		delete userThreads[index].getCond();
	}
	clear(index);
}

////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////
//								PUBLIC											  //

/**
 * fonction de synchronisation de la structure, a utiliser autours des manipulations
 */
void ThreadGroup::lock() {
	lock_Thread->Acquire();
}

void ThreadGroup::unlock() {
	lock_Thread->Release();
}

/**
 *	attends sur la condition propre a un thread renseigne via l'id
 */
void ThreadGroup::waitCond(int id) {
	int index;
	if((index = find(id)) == -1){
		fprintf(stderr,"\nunknown id\n");
		(&ProcessusGroup::Instance())->KillProg(currentThread->space->getPid());
	}
	if(userThreads[index].getEnd() == 1){
		fprintf(stderr,"\nTHREAD : no running id\n");
		(&ProcessusGroup::Instance())->KillProg(currentThread->space->getPid());
	}
	userThreads[index].getCond()->Wait(lock_Thread);
}

/**
 * indique la place d'un thread dans la pile du processus
 */
int ThreadGroup::findNum(int id){
	int index = find(id);
	if(index == -1){
		fprintf(stderr,"\nTHREAD : unknown id\n");
		(&ProcessusGroup::Instance())->KillProg(currentThread->space->getPid());
	}
	if(userThreads[index].getEnd()){
		fprintf(stderr,"\nTHREAD : no running id\n");
		(&ProcessusGroup::Instance())->KillProg(currentThread->space->getPid());
	}
	return userThreads[index].getNum();
}

/**
 * indique si un thread s'est fini ou pas, renvoi -1 si thread inconnu
 */
int ThreadGroup::isFinished(int id){
	int index = find(id);
	if(index == -1){
		return -1;
	}
	return userThreads[index].getEnd() == 1;
}

/**
 * fonction qui permet de mettre a jour la structure en finissant un thread et en enregistrant son retour
 */
void ThreadGroup::finished(int id,int ret) {
	int index = find(id);
	if(index == -1 ){
		fprintf(stderr,"\nTHREAD : on finished : unknown id\n");
		(&ProcessusGroup::Instance())->KillProg(currentThread->space->getPid());
	}
	if(userThreads[index].getEnd() == 1){
		fprintf(stderr,"\nTHREAD : on finished : bad id, no running thread\n");
		(&ProcessusGroup::Instance())->KillProg(currentThread->space->getPid());
	}
	userThreads[index].getCond()->Signal(lock_Thread);
	delete userThreads[index].getCond();
	userThreads[index].setEnd(1);
	userThreads[index].setRet(ret);
	userThreads[index].setThread(NULL);
	userThreads[index].clearBitMap();
	stackPlace->Clear(userThreads[index].getNum());
	nbThreadsRunning--;
}

/**
 * indique le nombre de thread en cours d'execution
 */
int ThreadGroup::getNbThreads() {
	return nbThreadsRunning;
}

/**
 * renvoi la taille de la structure
 */
int ThreadGroup::getTaille() {
	return taille;
}

/**
 * ajoute un thread a la structure si il y a la place, renvoi -1 si non
 */
int ThreadGroup::ajoutThread(){
	if(nbThreadsRunning + 1 > nbThreadsRunningMax){
		return -1;
	}
	if(redimension() == 0){
		return -1;
	}
	int index = ensembleThread->Find();
	int num = stackPlace->Find();
	int id = idGen;
	userThreads[index].setId(idGen);
	userThreads[index].setCond(new Condition(""));
	userThreads[index].setEnd(0);
	userThreads[index].setNum(num);
	add();
	return id;
}

/**
 * recupere le retour d'un thread et libere sa memoire
 */
int ThreadGroup::recupRet(int id){
	int ret = userThreads[find(id)].getRet();
	free(find(id));
	return ret;
}

/**
 *	renvoi le vrai id du fichier demande
 */
int ThreadGroup::getFile(int id,int fid){
	return userThreads[find(id)].getFile(fid);
}

/**
 *	ajoute une entree a l'ensemble de fichiers ouverts du thread donne
 */
int ThreadGroup::addFile(int id, int fid){
	return userThreads[find(id)].addFile(fid);
}

/**
 *	supprime l'entree donne dans l'ensemble de fichiers ouverts du thread donne
 */
int ThreadGroup::removeFile(int id,int fid){
	if(userThreads[find(id)].removeFile(fid)){
		fprintf(stderr,"\nTHREAD : unknown fid\n");
		(&ProcessusGroup::Instance())->KillProg(currentThread->space->getPid());
		return 0;
	}
	return 1;
}

void ThreadGroup::setThread(int id,Thread *t){
	userThreads[find(id)].setThread(t);
}

/**
 *	indique si un thread peut ajouter un fichier a son ensemble de fichiers ouverts
 */
int ThreadGroup::canAddFile(int id){
	return userThreads[find(id)].getNbFile() + 1 <= MAX_OPEN_FILES_THREAD;
}

////////////////////////////////////////////////////////////////////////////////////

/**
* indique le nombre de Thread possible en fonction de leur taille et de la taille de la pile utilisateur
*/
int nbMaxThread() {
	return (UserStackSize / THREAD_SIZE);
}

/**
 * initialise la structure pour le thread courant
 */
void initializeThread(){
	currentThread->space->setThreads(new ThreadGroup(nbMaxThread()));
}

////////////////////////////////////////////////////////////////////////////////////
//							FONCTION APPEL SYSTEME								  //

static void startThread(int a) {
	ArgumentThread *argThread = (ArgumentThread *)a;
	currentThread->space->InitRegisters();
	currentThread->space->RestoreState();

	unsigned int ad = argThread->addr;
	machine->WriteRegister(StackReg, ad);
	machine->WriteRegister(PrevPCReg, PCReg);
	machine->WriteRegister(4, argThread->arg);
	machine->WriteRegister(PCReg, argThread->func);
	machine->WriteRegister(NextPCReg, argThread->func+4);
	machine->WriteRegister(RetAddrReg, argThread->ret_addr);

	delete argThread;

	machine->Run();
}

int do_ThreadCreate(int f,int arg) {
	int id;
 	currentThread->space->getThreads()->lock();
	id = currentThread->space->getThreads()->ajoutThread();
	
	if(id == -1){
		currentThread->space->getThreads()->unlock();
		return -1;
	}

	unsigned int addr = currentThread->space->GetStackInit() - THREAD_SIZE * (currentThread->space->getThreads()->findNum(id));
	
	Thread *t = new Thread("");
	currentThread->space->getThreads()->setThread(id,t);

	currentThread->space->getThreads()->unlock();

	

	//Défini le nouveau nom du thread
	char name[MAX_NAME_SIZE];
	snprintf(name, MAX_NAME_SIZE, "%s_%d", currentThread->getName(), id);
	t->idThread = id;
	t->setName(name);

	ArgumentThread *argThread = new ArgumentThread(arg,machine->ReadRegister(6),f,addr);

	t->Fork(startThread, (int)argThread);
	return id;
}

/**
 * Fonction de fin de thread (par appel systeme)
 * Termine le thread courant et nettoie la bitmap
 */

void do_ThreadExit(int ret,int clear) {
	currentThread->space->getThreads()->lock();
	currentThread->space->getThreads()->finished(currentThread->idThread,ret);
	if(clear == 1){
		currentThread->space->getThreads()->recupRet(currentThread->idThread);
	}
	
	if(currentThread->space->getThreads()->getNbThreads() == 0){
		currentThread->space->getThreads()->unlock();
		(&ProcessusGroup::Instance())->processusRemove(currentThread->space);
		delete currentThread->space->getThreads();
		delete currentThread->space;
		//test si dernier processus en vie
		if((&ProcessusGroup::Instance())->noProcessusLeft()){
			//comme on éteint la machine on détruit les 2 singletons
			(&ProcessusGroup::Instance())->deleteProcessusGroup();
			(&FrameProvider::Instance())->DeleteFrameProvider();
			delete lockForkExec;
			interrupt->Halt();
		}
		currentThread->Finish();
	} else {
		currentThread->space->getThreads()->unlock();
		currentThread->Finish();
	}
}

int do_ThreadJoin(int id) {
	currentThread->space->getThreads()->lock();
	if(currentThread->space->getThreads()->isFinished(id) == 0){
		currentThread->space->getThreads()->waitCond(id);
	}
	else if(currentThread->space->getThreads()->isFinished(id) == -1){
		currentThread->space->getThreads()->unlock();
		fprintf(stderr,"\nTHREAD : on join, unknown id.\n");
		(&ProcessusGroup::Instance())->KillProg(currentThread->space->getPid());
		return -1;
	}
	int ret = currentThread->space->getThreads()->recupRet(id);
	currentThread->space->getThreads()->unlock();
	return ret;
}

////////////////////////////////////////////////////////////////////////////////////

int do_Create(char *name, int size){
	return fileSystem->Create(name, size);
}

int do_CreateDirectory(char *name){
	return fileSystem->CreateDirectory(name);
}

int do_Open(char *name){
	if(currentThread->space->getThreads()->canAddFile(currentThread->idThread)){
		int realFid = fileSystem->Open(name);
		if(realFid == -1){
			return -1;
		}
		int index = currentThread->space->getThreads()->addFile(currentThread->idThread,realFid);
		if(index == -1){
			fprintf(stderr,"\nTHREAD : add file error\n");
			return -3;
		}
		return index;
	}
	else{
		return -2;
	}
}

int do_Close (int fid){
	int realFid = currentThread->space->getThreads()->getFile(currentThread->idThread,fid);
	if(!currentThread->space->getThreads()->removeFile(currentThread->idThread,fid)){
		return fileSystem->Close(realFid);
	}
	return 0;
}

int do_Read(char *buf,int size,int fid){
	int realFid = currentThread->space->getThreads()->getFile(currentThread->idThread,fid);
	if(realFid == -1){
		fprintf(stderr,"\nTHREAD : unknown fid during READ\n");
		(&ProcessusGroup::Instance())->KillProg(currentThread->space->getPid());
	}
	return fileSystem->Read((const int)realFid, buf, size);
}

int do_Write(char *buf,int size,int fid){
	int realFid = currentThread->space->getThreads()->getFile(currentThread->idThread,fid);
	if(realFid == -1){
		fprintf(stderr,"\nTHREAD : unknown fid during WRITE\n");
		(&ProcessusGroup::Instance())->KillProg(currentThread->space->getPid());
	}
	return fileSystem->Write((const int)realFid, buf, size);
}

void do_KillSelf(int a){
	do_ThreadExit(a,1);
}

void ThreadGroup::killSelf(){
	int i;
	for(i = 0 ; i < taille ; i++){
		if(ensembleThread->Test(i) && userThreads[i].getEnd()){
			clear(i);
		}
		else if(ensembleThread->Test(i) && !userThreads[i].getEnd()){
			if(userThreads[i].getId() != currentThread->idThread){
				userThreads[i].killSelf();
			}
		}
	}
	currentThread->killSelf();
}

int do_Remove(char *name){
	return fileSystem->Remove(name);
}

int do_RemoveDirectory(char *name){
	return fileSystem->RemoveDirectory(name);
}

void do_List(){
	fileSystem->List();
}

int do_ChangeDirectory(char *path){
	return fileSystem->ChangeDirectory(path);
}

void do_PWD(){
	fileSystem->PWD();
}
