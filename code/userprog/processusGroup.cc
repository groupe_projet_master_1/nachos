#include "processusGroup.h"

#include "bitmap.h"
#include "synch.h"
#include "machine.h"
#include "system.h"
#include "frameprovider.h"
#include "userProcessus.h"

static FrameProvider *frameProvider = &FrameProvider::Instance();

ProcessusGroup ProcessusGroup::m_instance = ProcessusGroup();

ProcessusGroup& ProcessusGroup::Instance(void){
    return m_instance;
}

ProcessusGroup::ProcessusGroup() {
    nb = 0;
	lock_Processus = new Lock("all processus lock");
	cond_Processus = new Condition("all processus cond ");
    nbProcessusMax = 8;
    pool = new UserProcessus[nbProcessusMax];
    bit = new BitMap(nbProcessusMax);
    nbProcessus = 0;
};

void ProcessusGroup::deleteProcessusGroup() {
    delete cond_Processus;
	delete lock_Processus;
    delete [] pool;
    delete bit;
}

ProcessusGroup::~ProcessusGroup() {
};

void ProcessusGroup::redimension(){
    BitMap *bit_temp = new BitMap(nbProcessusMax + 8);
    UserProcessus *pool_temp = new UserProcessus[nbProcessusMax + 8];
    int i;
    for(i = 0; i < nbProcessusMax ; i++){
        if(bit->Test(i)){
             pool_temp[i] = pool[i];
             bit_temp->Mark(i);
        }
    }
    nbProcessusMax += 8;

    delete bit;
    delete [] pool;

    pool = pool_temp;
    bit = bit_temp;
}

int ProcessusGroup::addProcessus(int ppid){
    lock_Processus->Acquire();
    if(nbProcessus + 1 >= nbProcessusMax) {
        redimension();
    }
    int indice = bit->Find();

    
    pool[indice].setPid(nb);
    pool[indice].setPPid(ppid);
    nb++;
    nbProcessus++;
    lock_Processus->Release();
    return nb - 1;
}

void ProcessusGroup::processusRemove(AddrSpace *space){
    lock_Processus->Acquire();
    int indice;
    if((indice = indiceFromPid(space->getPid())) != -1){
        unsigned int i;
        for(i = 0; i < space->getNumPages(); i++){
            (&FrameProvider::Instance())->ReleaseFrame(space->getPageTable()[i].physicalPage);
        }
        bit->Clear(indice);
        nbProcessus--;
        broadcastCond();
    }
    else {
        fprintf(stderr,"Erreur : Mauvais pid enregistré : %d\n",space->getPid());
        ASSERT(FALSE);
    }
    lock_Processus->Release();
}

bool ProcessusGroup::noProcessusLeft(){
    bool b = true;
    lock_Processus->Acquire();
    b = (nbProcessus==0);
    lock_Processus->Release();
    return b;
}

bool ProcessusGroup::canInstanceProcess(int nbPages){
    int numberAvail = frameProvider->NumAvailFrame();
    return nbPages <= numberAvail;
}

void ProcessusGroup::waitCond(){
    cond_Processus->Wait(lock_Processus);
}

void ProcessusGroup::broadcastCond(){
    cond_Processus->Broadcast(lock_Processus);
}

int ProcessusGroup::indiceFromPid(int pid){
    int i;
    for(i = 0 ; i < nbProcessusMax ; i++){
        if(bit->Test(i) && pool[i].getPid() == pid){
            return i;
        }
    }
    return -1;
}

void ProcessusGroup::join(int pid){
    lock_Processus->Acquire();
    int present = 1;
    while(present){
        if(indiceFromPid(pid) != -1){
            waitCond();
        }
        else{
            present = 0;
        }
    }
    lock_Processus->Release();
}

void ProcessusGroup::setThreadGroup(int pid,ThreadGroup *t){
    pool[indiceFromPid(pid)].setThreadGroup(t);
}

void ProcessusGroup::KillProg(int pid){
    (void) interrupt->SetLevel (IntOff);
    ThreadGroup *t= pool[indiceFromPid(pid)].getThreadGroup();
    t->killSelf();
    (void) interrupt->SetLevel (IntOn);
}