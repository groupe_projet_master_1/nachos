// exception.cc 
//      Entry point into the Nachos kernel from user programs.
//      There are two kinds of things that can cause control to
//      transfer back to here from user code:
//
//      syscall -- The user code explicitly requests to call a procedure
//      in the Nachos kernel.  Right now, the only function we support is
//      "Halt".
//
//      exceptions -- The user code does something that the CPU can't handle.
//      For instance, accessing memory that doesn't exist, arithmetic errors,
//      etc.  
//
//      Interrupts (which can also cause control to transfer from user
//      code into the Nachos kernel) are handled elsewhere.
//
// For now, this only handles the Halt() system call.
// Everything else core dumps.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "threadGroup.h"
#include "directory.h"
#include "processusGroup.h"
#include "userSem.h"
#include "userprog.h"
#include "synch.h"
#include "syscall.h"
#include "system.h"
#include "network.h"

#include "post.h"

static Lock *reseauLock = new Lock("reseau");

extern void StartProcess (char *file);


//Le semaphore empeche l'excecution du main trouve pq
//static Semaphore* protectExcep;
//----------------------------------------------------------------------
// UpdatePC : Increments the Program Counter register in order to resume
// the user program immediately after the "syscall" instruction.
//----------------------------------------------------------------------
static void UpdatePC (int u) {
  if(u){
    int pc = machine->ReadRegister (PCReg);
    machine->WriteRegister (PrevPCReg, pc);
    pc = machine->ReadRegister (NextPCReg);
    machine->WriteRegister (PCReg, pc);
    pc += 4;
    machine->WriteRegister (NextPCReg, pc);
  }
  else{
    printf("non update PC, le thread est lance sur %d et son adresse de retour est %d\n",machine->ReadRegister (PCReg),machine->ReadRegister (RetAddrReg));
  }
}

static int copyStringFromMachine(int from, char *to, unsigned size) {
  unsigned i;
  int ch = -1;
  for(i = 0; i < size - 1 && (char)ch != '\0'; i++) {
    machine->ReadMem(from + i, 1, &ch);
    to[i] = (char)ch;
  }
  to[i] = '\0';
  return i;
}

static void copyStringToMachine(char* from, int to, unsigned size) {
  unsigned i;
  for(i = 0; i < size && from[i] != '\0'; i++) {
    machine->WriteMem(to + i, 1, (unsigned int)from[i]);
  }
  machine->WriteMem(to + i, 1, 0);
}

//----------------------------------------------------------------------
// ExceptionHandler
//      Entry point into the Nachos kernel. Called when a user program
//      is executing, and either does a syscall, or generates an addressing
//      or arithmetic exception.
//
//      For system calls, the following is the calling convention:
//
//      system call code -- r2
//              arg1 -- r4
//              arg2 -- r5
//              arg3 -- r6
//              arg4 -- r7
//
//      The result of the system call, if any, must be put back into r2. 
//
// And don't forget to increment the pc before returning. (Or else you'll
// loop making the same system call forever!
//
//      "which" is the kind of exception.  The list of possible exceptions 
//      are in machine.h.
//----------------------------------------------------------------------

void ExceptionHandler (ExceptionType which) {
    int type = machine->ReadRegister(2);
    int from, toGetString, n, f, arg, nbChar, totalChar,id,ret;
    int update = 1;
    char fromGetString[MAX_STRING_SIZE];
    char to[MAX_STRING_SIZE];
    //char* to = (char*) malloc(MAX_STRING_SIZE*sizeof(char));

    char charToPrint;
    int intToPrint;

    if (which == SyscallException) {
      switch(type) {
        case SC_Halt:
            DEBUG ('s', "Shutdown, initiated by user program\n");
            interrupt->Halt();
          break;
        case SC_Yield:
            DEBUG ('s', "Yield\n");
            currentThread->Yield();
          break;
        case SC_ProcJoin:
            DEBUG ('s', "Process join\n");
            do_ProcJoin(machine->ReadRegister(4));
          break;
        case SC_Create:
            DEBUG ('s', "Create\n");
            arg = machine->ReadRegister(4);
            n = machine->ReadRegister(5);
            copyStringFromMachine(arg, to, FileNameMaxLen + 1);
            n = do_Create(to, n);
            machine->WriteRegister(2, n);
          break;
        case SC_Open:
            DEBUG ('s', "Open\n");
            arg = machine->ReadRegister(4);
            copyStringFromMachine(arg, to, FileNameMaxLen + 1);
            n = do_Open(to);
            machine->WriteRegister(2, n);
          break;
        case SC_Read:
            DEBUG ('s', "Read\n");
            arg = machine->ReadRegister(4);
            n = machine->ReadRegister(5);
            f = machine->ReadRegister(6);
            n = do_Read(to, n, f);
            copyStringToMachine(to, arg, n);
            machine->WriteRegister(2, n);
          break;
        case SC_Write:
            DEBUG ('s', "Write\n");
            arg = machine->ReadRegister(4);
            n = machine->ReadRegister(5);
            f = machine->ReadRegister(6);
            nbChar = 0;
            totalChar = 0;
            synchconsole->lockLong();
            // Ecrit la chaîne de caractères en plusieurs parties de taille n
            do {
              nbChar = copyStringFromMachine(arg + totalChar, to, n);
              DEBUG ('s', " - %s / %d", to, nbChar);
              nbChar = do_Write(to, nbChar, f);
              totalChar += nbChar;
            } while (nbChar == n - 1);
            DEBUG ('s', "\n");
            synchconsole->unlockLong();
            machine->WriteRegister(2, totalChar);
          break;
        case SC_Close:
            DEBUG ('s', "Close\n");
            arg = machine->ReadRegister(4);
            n = do_Close(arg);
            machine->WriteRegister(2, n);
          break;
        case SC_Remove:
            DEBUG ('s', "Remove\n");
            arg = machine->ReadRegister(4);
            copyStringFromMachine(arg, to, FileNameMaxLen + 1);
            n = do_Remove(to);
            machine->WriteRegister(2, n);
          break;
        case SC_CreateDir:
            DEBUG ('s', "Create directory\n");
            arg = machine->ReadRegister(4);
            copyStringFromMachine(arg, to, FileNameMaxLen + 1);
            n = do_CreateDirectory(to);
            machine->WriteRegister(2, n);
          break;
        case SC_ChangeDir:
            DEBUG('s', "Change directory\n");
            arg = machine->ReadRegister(4);
            copyStringFromMachine(arg, to, FileNameMaxLen + 1);
            n = do_ChangeDirectory(to);
            machine->WriteRegister(2, n);
          break;
        case SC_RemoveDir:
            DEBUG ('s', "Remove directory\n");
            arg = machine->ReadRegister(4);
            copyStringFromMachine(arg, to, FileNameMaxLen + 1);
            n = do_RemoveDirectory(to);
            machine->WriteRegister(2, n);
          break;
        case SC_List:
            DEBUG('s', "List\n");
            do_List();
          break;
        case SC_PWD:
            DEBUG('s', "PWD\n");
            do_PWD();
          break;
        case SC_GetChar:
            DEBUG ('s', "Get char\n");
            machine->WriteRegister(2, (int) synchconsole->SynchGetChar());
          break;
        case SC_GetInt:
            DEBUG ('s', "Get int\n");

            machine->WriteRegister(2, synchconsole->SynchGetInt(&n));
            machine->WriteMem(machine->ReadRegister(4), 4, n);
          break;
        case SC_GetString:
            DEBUG ('s', "Get string\n");
            toGetString = machine->ReadRegister(4);
            n = machine->ReadRegister(5);
            synchconsole->SynchGetString(fromGetString, n);
            copyStringToMachine(fromGetString, toGetString, n);
          break;
        case SC_PutChar:
            charToPrint = (char)machine->ReadRegister(4);
            DEBUG ('s', "Put char %c by thread %d of processus %d\n", charToPrint,currentThread->idThread,currentThread->space->getPid());
            synchconsole->SynchPutChar(charToPrint);
          break;
        case SC_PutInt:
            intToPrint = machine->ReadRegister(4);
            DEBUG ('s', "Put int %d\n", intToPrint);
            synchconsole->SynchPutInt(intToPrint);
          break;
        case SC_PutString:
            DEBUG ('s', "Put string\n");
            // On recupere la valeur du registre
            from = machine->ReadRegister(4);
            nbChar = 0;
            totalChar = 0;
            synchconsole->lockLong();
            // Affiche la chaîne de caractères en plusieurs parties de taille MAX_STRING_SIZE
            do {
              nbChar = copyStringFromMachine(from + totalChar, to, MAX_STRING_SIZE);
              totalChar += nbChar;
              synchconsole->SynchPutString(to);
            } while (nbChar == MAX_STRING_SIZE - 1);
            synchconsole->unlockLong();
          break;
        case SC_ThreadCreate:
            DEBUG ('s', "Thread create\n");
            f = machine->ReadRegister(4);
            arg = machine->ReadRegister(5);
            machine->WriteRegister(2, do_ThreadCreate(f, arg));
          break;
        case SC_ThreadExit:
            DEBUG ('s', "Thread exit\n");
            do_ThreadExit(machine->ReadRegister(4),machine->ReadRegister(5));
          break;
        case SC_ThreadJoin:
            DEBUG ('s', "Thread join\n");
            id = machine->ReadRegister(4);
            ret = do_ThreadJoin(id);
            machine->WriteRegister(2,ret);
          break;
        case SC_SemInit:
            DEBUG ('s', "SemInit avec %d valeur initiale\n",machine->ReadRegister(4));
            machine->WriteRegister(2,do_initSem(machine->ReadRegister(4)));
          break;
        case SC_SemClear:
            DEBUG ('s', "SemClear du semaphore n°%d\n",machine->ReadRegister(4));
            do_clearSem(machine->ReadRegister(4));
          break;
        case SC_P:
            DEBUG ('s', "P sur le semaphore n°%d\n",machine->ReadRegister(4));
            do_P(machine->ReadRegister(4));
          break;
        case SC_V:
            DEBUG ('s', "V sur le semaphore n°%d\n",machine->ReadRegister(4));
            do_V(machine->ReadRegister(4));
          break;
        case SC_ForkExec:
            DEBUG ('s', "Fork Exec\n");
            from = machine->ReadRegister(4);
            copyStringFromMachine(from, to, MAX_STRING_SIZE);
            if((n = do_ForkExec(to)) == -1){
              fprintf(stderr,"Erreur lors de la creation du processus\n");
            }
            machine->WriteRegister(2, n);
          break;
        case SC_Exit:
            //R4 contient le paramètre de retour (parametre dans start.s)
            DEBUG('s', "Fin de programme\nCode de retour : %d\n", machine->ReadRegister(4));
            do_ThreadExit(machine->ReadRegister(4),1);
          break;
        case SC_Receive:
          {
            DEBUG('s', "Reception\n");
            int farAddr = (int)machine->ReadRegister(4);
            char *data = (char *)machine->ReadRegister(5);
            int size = (int)machine->ReadRegister(6);

            PacketHeader inPktHdr;
            MailHeader inMailHdr;
            char buffer[MaxMessageSize];
            postOffice->Receive(farAddr, &inPktHdr, &inMailHdr, buffer);
            int size2 = size + 1;
            copyStringToMachine(buffer, (int) data, size2);
            break;
          }

        case SC_Send:
          {
            reseauLock->Acquire();
            DEBUG('s', "Envoi\n");
            int farAddr = (int)machine->ReadRegister(4);
            from = machine->ReadRegister(5);
            char data[MaxMessageSize]; 
            copyStringFromMachine(from, data, MaxMessageSize);

            NetworkAddress myAddr = postOffice->getNetAddr();
            PacketHeader outPktHdr;
            MailHeader outMailHdr;
            outPktHdr.to = farAddr;		
            outMailHdr.to = farAddr;
            outMailHdr.from = myAddr;
            outMailHdr.length = strlen(data) + 1;
            outMailHdr.idPIDSender = 0; //Pas de reception

            ret = postOffice->SecureSend(outPktHdr, outMailHdr, data);
            if(ret != 0)
            {
              ret = 1;
            }
            machine->WriteRegister(2, ret);
            reseauLock->Release();
            break;
          }
        case SC_SendFile:
          {
            reseauLock->Acquire();
            DEBUG('s', "Envoi Fichier\n");
            int farAddr = (int)machine->ReadRegister(4);
            from = machine->ReadRegister(5);
            char fileName[MaxMessageSize];
            copyStringFromMachine(from, fileName, MaxMessageSize);

            NetworkAddress myAddr = postOffice->getNetAddr();
            PacketHeader outPktHdr;
            MailHeader outMailHdr;
            outPktHdr.to = farAddr;		
            outMailHdr.to = farAddr;
            outMailHdr.from = myAddr;
            outMailHdr.length = strlen(fileName) + 1;
            outMailHdr.idPIDSender = 0; //Pas de reception
            
            ret = postOffice->SendFile(outPktHdr, outMailHdr, fileName);
            machine->WriteRegister(2, ret);
            reseauLock->Release();
            break;
          }
        case SC_GetFile:
          {
            reseauLock->Acquire();
            DEBUG('s', "Demande de fichier\n");
            int farAddr = (int)machine->ReadRegister(4);
            from = machine->ReadRegister(5);
            char fileName[MaxMessageSize];
            copyStringFromMachine(from, fileName, MaxMessageSize);

            NetworkAddress myAddr = postOffice->getNetAddr();
            PacketHeader outPktHdr;
            MailHeader outMailHdr;
            outPktHdr.to = farAddr;		
            outMailHdr.to = farAddr;
            outMailHdr.from = myAddr;
            outMailHdr.length = strlen(fileName) + 1;
            outMailHdr.idPIDSender = 0; //Pas de reception
            
            ret = postOffice->GetFile(outPktHdr, outMailHdr, fileName);
            machine->WriteRegister(2, ret);
            break;
          }
        default:
            printf ("Type of SyscallException (%d) unmanaged\n", type);
            ASSERT (FALSE);
      }
    } else {
      printf("ExceptionType not ok : %d (should be %d)\n", which, SyscallException);
      printf("Type contain in R2 : %d\n", type);
	    ASSERT (FALSE);
    }

    //free(to);
    // LB: Do not forget to increment the pc before returning!
    UpdatePC (update);
    // End of addition
}