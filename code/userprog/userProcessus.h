#ifndef __USER_PROCESSUS_H__
#define __USER_PROCESSUS_H__


class UserProcessus {
	private:
		int pid;
		int ppid;
		ThreadGroup *threadGroup;
	public:	//Getter
		UserProcessus(){};
		~UserProcessus(){};
		int getPPid() {
			return ppid;
		};
		int getPid() {
			return pid;
		};
		ThreadGroup *getThreadGroup(){
			return threadGroup;
		} 
		void setPid(int id){
			pid = id;
		};
		void setPPid(int id){
			ppid = id;
		};
		void setThreadGroup(ThreadGroup *t){
			threadGroup = t;
		}

};

#endif // __USER_THREAD_H__