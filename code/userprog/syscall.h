/* syscalls.h 
 * 	Nachos system call interface.  These are Nachos kernel operations
 * 	that can be invoked from user programs, by trapping to the kernel
 *	via the "syscall" instruction.
 *
 *	This file is included by user programs and by the Nachos kernel. 
 *
 * Copyright (c) 1992-1993 The Regents of the University of California.
 * All rights reserved.  See copyright.h for copyright notice and limitation 
 * of liability and disclaimer of warranty provisions.
 */

#ifndef SYSCALLS_H
#define SYSCALLS_H

#include "copyright.h"


/* system call codes -- used by the stubs to tell the kernel which system call
 * is being asked for
 */
#define SC_Halt		    0 
#define SC_Exit		    1
#define SC_Exec		    2
#define SC_ProcJoin		3
#define SC_Create	    4
#define SC_CreateDir    5
#define SC_Open		    6
#define SC_Read		    7
#define SC_Write	    8
#define SC_Close	    9
#define SC_Fork		    10
#define SC_Yield	    11
#define SC_Remove       12
#define SC_RemoveDir    13
#define SC_List         14
#define SC_ChangeDir    15
#define SC_PWD          16

/* I/O  */
#define SC_PutChar      17
#define SC_PutString    18
#define SC_PutInt       19
#define SC_GetChar      20
#define SC_GetString    21
#define SC_GetInt       22

/* Thread  */
#define SC_ThreadCreate 23
#define SC_ThreadExit   24
#define SC_ThreadJoin   25

/* Semaphore */
#define SC_SemInit  	26
#define SC_SemClear  	27
#define SC_P  			28
#define SC_V   			29


/*Etape 4*/
#define SC_ForkExec     30

/*Etape 6*/
#define SC_Send         31
#define SC_Receive      32
#define SC_SendFile     33
#define SC_GetFile      34

#ifdef IN_USER_MODE

// LB: This part is read only on compiling the test/*.c files.
// It is *not* read on compiling test/start.S


/* The system call interface.  These are the operations the Nachos
 * kernel needs to support, to be able to run user programs.
 *
 * Each of these is invoked by a user program by simply calling the 
 * procedure; an assembly language stub stuffs the system call code
 * into a register, and traps to the kernel.  The kernel procedures
 * are then invoked in the Nachos kernel, after appropriate error checking, 
 * from the system call entry point in exception.cc.
 */

/* Stop Nachos, and print out performance stats */
void Halt () __attribute__((noreturn));


/* Address space control operations: Exit, Exec, and Join */

/* This user program is done (status = 0 means exited normally). */
void Exit (int status) __attribute__((noreturn));

/* A unique identifier for an executing user program (address space) */
typedef int SpaceId;

/* Run the executable, stored in the Nachos file "name", and return the 
 * address space identifier
 */
SpaceId Exec (char *name);

/* Only return once the the user program "id" has finished.  
 * Return the exit status.
 */
int ProcJoin (int pid);


/* File system operations: Create, Open, Read, Write, Close
 * These functions are patterned after UNIX -- files represent
 * both files *and* hardware I/O devices.
 *
 * If this assignment is done before doing the file system assignment,
 * note that the Nachos file system has a stub implementation, which
 * will work for the purposes of testing out these routines.
 */

/* A unique identifier for an open Nachos file. */
typedef int OpenFileId;


typedef int sem_t;

typedef int thread_t;

/* when an address space starts up, it has two open files, representing 
 * keyboard input and display output (in UNIX terms, stdin and stdout).
 * Read and Write can be used directly on these, without first opening
 * the console device.
 */

#define ConsoleInput	0
#define ConsoleOutput	1

/* Create a Nachos file, with "name" */
int Create (char *name, int size);

/* Create a Nachos directory, with "name" */
int CreateDirectory (char *name);

/* Open the Nachos file "name", and return an "OpenFileId" that can 
 * be used to read and write to the file.
 */
OpenFileId Open (char *name);

/* Write "size" bytes from "buffer" to the open file. */
int Write (char *buffer, int size, OpenFileId id);

/* Read "size" bytes from the open file into "buffer".  
 * Return the number of bytes actually read -- if the open file isn't
 * long enough, or if it is an I/O device, and there aren't enough 
 * characters to read, return whatever is available (for I/O devices, 
 * you should always wait until you can return at least one character).
 */
int Read (char *buffer, int size, OpenFileId id);

/* Close the file, we're done reading and writing to it. 
 * Return 1 if succesful, 0 otherwise.
 */
int Close (OpenFileId id);

/* Remove a file given its path.
 * Return 1 if succesful, 0 otherwise.
 */
int Remove (char *path);

/* Remove an empty directory given its path.
 * Return 1 if succesful, 0 otherwise.
 */
int RemoveDirectory (char *path);

/*
 * List all files in working directory.
 */
void List ();

/*
 * Change working directory to path.
 */
int ChangeDirectory (const char *path);

/*
 * Print name of current working directory.
 */
char *PWD();

/* User-level thread operations: Fork and Yield.  To allow multiple
 * threads to run within a user program. 
 */

/* Fork a thread to run a procedure ("func") in the *same* address space 
 * as the current thread.
 */
void Fork (void (*func) ());

/* Yield the CPU to another runnable thread, whether in this address space 
 * or not. 
 */
void Yield ();


/////////////////////////////////////////////////////////////////////////////////
/* I/O  */


/* Affiche un charactere grace a SynchPutChar
 */
void PutChar(char c);

/* Affiche une chaine de characteres grace a SynchPutString
 */
void PutString(char* s);

/* Affiche un entier
 */
void PutInt(int n);

/* Recupere un charactere grace a SynchGetChar
 */
char GetChar();

/* Recupere une chaine de charactere grace a SynchGetString
 */
void GetString(char* addr, int size);

/* Recupere un entier
 */
int GetInt(int *n);

/////////////////////////////////////////////////////////////////////////////////
/* Thread  */


/* cree un thread utilisateur
 */
thread_t ThreadCreate(void f(void *arg),void *arg);

/* Fini un thread utilisateur
 */
void ThreadExit(int ret);

/* attends qu'un thread utilisateur identifie par id se finisse
 */
int ThreadJoin(int id);

/////////////////////////////////////////////////////////////////////////////////
/* Semaphore */


/* initialise un nouveau semaphore
 */
sem_t SemInit(int initValue);

/* Prends le semaphore si disponible
 */
void P(sem_t s);

/* relache le semaphore
 */
void V(sem_t s);

/* libere la memoire du semaphore
 */
void SemClear(sem_t s);

/////////////////////////////////////////////////////////////////////////////////

/* Cree un thread au niveau systeme avec le programme s
*/
int ForkExec(char *s);

////////////////////////////////////////////////////////////////////////////////
/* Réseau */

int Send(int idMachine, char *data);

void Receive(int idMachine, char *buffer, int size);

int SendFile(int idMachine, char *fileName);

int GetFile(int idMachine, char *fileName);

#endif // IN_USER_MODE

#endif /* SYSCALL_H */
