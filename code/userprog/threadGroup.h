#ifndef __POOL_THREAD_H__
#define __POOL_THREAD_H__

#include "userThread.h"
#include "thread.h"


#define TAILLE_MAX_ENSEMBLE_THREAD 64

class Lock;
class BitMap;
class Condition;
class Thread;
class UserThread;

extern int do_ThreadCreate(int f, int arg);
extern void do_ThreadExit(int ret,int clear);
extern int do_ThreadJoin(int id);
extern int ajoutThread();
extern void initializeThread();
extern void do_KillSelf(int a);

extern int do_Create(char *name, int size);
extern int do_CreateDirectory(char *name);
extern int do_Open(char *name);
extern int do_Close (int fid);
extern int do_Read(char *buf,int size,int fid);
extern int do_Write(char *buf,int size,int fid);
extern int do_Remove(char *name);
extern int do_RemoveDirectory(char *name);
extern void do_List();
extern int do_ChangeDirectory(char *path);
extern void do_PWD();

class ArgumentThread{
	public:
		ArgumentThread(int a,int r,int f,unsigned int ad){
			arg = a;
			ret_addr = r;
			func = f;
			addr = ad;
		}
		~ArgumentThread(){};
		int arg;
		int ret_addr;
		int func;
		unsigned int addr;
};

/**
 * Structure de controle des threads d'un processus
 * Un thread est enregistre dans la structure via ajoutThread, et declare termine grace a finished
 * Certaines informations reste quand le thread est fini si aucun ThreadExit n'est rencontre,
 * tel que le retour donne avec ThreadExit, l'id, et l'indicateur que le thread est fini.
 * Pour laisser la place a d'autre thread, il suffit d'utiliser le retour grace au ThreadJoin
 * Si ThreadExit, alors ThreadJoin, jamais l'un sans l'autre
 * Bien liberer la place car le nombre de thread conserve est limite
 */
class ThreadGroup {
private:
	int idGen;
	int nbThreadsTotal;
	int nbThreadsRunning;
	int nbThreadsRunningMax;
	int taille;
	BitMap *ensembleThread;
	BitMap *stackPlace;
	Lock *lock_Thread;
	UserThread *userThreads;
	int redimension();
	void add();
	void clear(int index);
	void free(int index);
	int find(int id);

public:
	ThreadGroup(int nbThreadMax);
	~ThreadGroup();
	void lock();
	void unlock();
	void waitCond(int id);
	void finished(int index,int ret);
	int isFinished(int id);
	int getNbThreads();
	int getTaille();
	int ajoutThread();
	void setThread(int id,Thread *t);
	int recupRet(int id);
	int findNum(int id);
	int getFile(int id,int fid);
	int addFile(int id, int fid);
	int removeFile(int id,int fid);
	int canAddFile(int id);
	void killSelf();
};

#endif // __POOL_THREAD_H__