#ifndef __USER_SEM_H__
#define __USER_SEM_H__

#define MAX_NB_SEM 	30

#define SEM_FAIL 	-1

class BitMap;
class Lock;
class Condition;
class Semaphore;

extern int do_initSem(int initValue);
extern void do_clearSem(int indice);
extern void do_P(int indice);
extern void do_V(int indice);

class UserSem {
	private:
		int nbSemMax;
		int nbSem;
		BitMap *group;
		Lock *lock_Sem;
		Semaphore *SemGroup;
		int redimension();
	
	public:	
		UserSem();
		~UserSem();
		int addSem(int initValue);
		void P(int indice);
		void V(int indice);
		void removeSem(int indice);
};

#endif // __USER_SEM_H__