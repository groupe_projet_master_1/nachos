#ifndef FRAMEPROVIDER_H
#define FRAMEPROVIDER_H

#include "machine.h"
#include "bitmap.h"
#include "synch.h"

#define FRAME_RANDOMIZER_F_NORMALE 0
#define FRAME_RANDOMIZER_F_RAND 1


#define FRAME_RANDOMIZER FRAME_RANDOMIZER_F_NORMALE



// Methode du singleton

class FrameProvider
{
  public:
    static FrameProvider &Instance(void);
    int GetEmptyFrame(void);             
    void ReleaseFrame(int id);           
    int NumAvailFrame(void);              
    
    void DeleteFrameProvider(void);

    int FrameRandomizerNorm();
    int FrameRandomizerRand();
  private:
    FrameProvider(void);
    ~FrameProvider(void);

    //Pour avoir une unique instance de FrameProvider
    static FrameProvider m_instance;
    Lock *m_lock;
    BitMap *m_bitmap;
};


#endif
