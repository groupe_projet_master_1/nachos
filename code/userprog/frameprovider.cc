#include "frameprovider.h"
#include "machine.h"

#include <cstdlib> // srand, rand
#include <ctime>
#include <utility> // std::swap

extern Machine *machine;

#if (FRAME_RANDOMIZER == FRAME_RANDOMIZER_F_RAND)
static int randomFrames[NumPhysPages];

static void initRandomFrames(void){
    int i, j;
    srand(time(NULL));

    for(i = 0; i < NumPhysPages; i++){
        randomFrames[i] = i;
    }

    for(i = 0; i < NumPhysPages; i++) {
        j = rand() % (NumPhysPages - i) + i;
        std::swap(randomFrames[i], randomFrames[j]);
    }
    
    return;
}
#endif

FrameProvider FrameProvider::m_instance = FrameProvider();

FrameProvider::FrameProvider(void){
    m_lock = new Lock("frame provider lock");
    m_bitmap = new BitMap(NumPhysPages);

#if (FRAME_RANDOMIZER == FRAME_RANDOMIZER_F_RAND)
    initRandomFrames();
#endif

}

FrameProvider::~FrameProvider(void) {
}

FrameProvider& FrameProvider::Instance(void){
    return m_instance;
}



int FrameProvider::GetEmptyFrame(void){
    int id;

    m_lock->Acquire();

    //id = m_bitmap->Find();
#if (FRAME_RANDOMIZER == FRAME_RANDOMIZER_F_NORMALE)
    id = FrameRandomizerNorm();
#elif (FRAME_RANDOMIZER == FRAME_RANDOMIZER_F_RAND)
    id = FrameRandomizerRand();
#else
    #error "FRAME_RANDOMIZER mal initialise"
#endif


    // Si de la place on efface la page que l'on va donner (on s'assure qu'elle est vide)
    if(id != -1){
        bzero(&(machine->mainMemory[PageSize * id]), PageSize);
    }

    m_lock->Release();

    return id;
}

void FrameProvider::ReleaseFrame(int id) {
    m_lock->Acquire();

#if (FRAME_RANDOMIZER == FRAME_RANDOMIZER_F_RAND)
    int i;
    for(i = 0; i < NumPhysPages; i++){
        if(randomFrames[i] == id) {
            id = i;
            break;
        }
    }
#endif


    m_bitmap->Clear(id);

    m_lock->Release();

    return;
}

int FrameProvider::FrameRandomizerNorm(){
    return m_bitmap->Find();
}

int FrameProvider::FrameRandomizerRand(){
#if (FRAME_RANDOMIZER == FRAME_RANDOMIZER_F_RAND)
    return randomFrames[m_bitmap->Find()];
#else
    return 0;
#endif
}



int FrameProvider::NumAvailFrame(void) {
    m_lock->Acquire();
    int n = m_bitmap->NumClear();
    m_lock->Release();
    return n;
}


void FrameProvider::DeleteFrameProvider(void){
    delete m_lock;
    delete m_bitmap;
}