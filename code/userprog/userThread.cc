#include "userThread.h"
#include "bitmap.h"
#include "system.h"

UserThread::UserThread(){ 
	bitFiles = new BitMap(MAX_OPEN_FILES_THREAD);
	nbFile = 0;
};

UserThread::~UserThread(){ 
	delete bitFiles;
};

int UserThread::addFile(int fid){
	int index = bitFiles->Find();
	if(index == -1){
		return -1;
	}
	files[index] = fid;
	nbFile += 1;
	return index;
}

int UserThread::getFile(int index){
	if(bitFiles->Test(index)){
		return files[index];
	}
	else{
		return -1;
	}
}

int UserThread::removeFile(int index){
	if(bitFiles->Test(index)){
		bitFiles->Clear(index);
		nbFile -= 1;
		return 0;
	}
	else{
		return -1;
	}
}

void UserThread::clearBitMap(){
	int i;
	for(i = 0 ; i < MAX_OPEN_FILES_THREAD ; i++){
		bitFiles->Clear(i);
	}
}

void UserThread::killSelf(){
	Thread *t = getThread();
	t->killSelf();
}