// system.h 
//      All global variables used in Nachos are defined here.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#ifndef SYSTEM_H
#define SYSTEM_H

#include "copyright.h"
#include "utility.h"
#include "thread.h"
#include "scheduler.h"
#include "interrupt.h"
#include "stats.h"
#include "timer.h"
#include "post.h"

// Initialization and cleanup routines
extern void Initialize (int argc, char **argv);	// Initialization,
						// called before anything else
extern void Cleanup ();		// Cleanup, called when
						// Nachos is done.

class PostOffice;
extern Thread *currentThread;	// the thread holding the CPU
extern Thread *threadToBeDestroyed;	// the thread that just finished
extern Scheduler *scheduler;	// the ready list
extern Interrupt *interrupt;	// interrupt status
extern Statistics *stats;	// performance metrics
extern Timer *timer;		// the hardware alarm clock

#define MaxMessageSize (1024)
extern PostOffice *postOffice;

#define MAIN_THREAD "main"

#define MAX_INT 2147483647
#define MIN_INT -2147483647

#ifdef USER_PROGRAM
#include "machine.h"
#include "synchconsole.h"
#include "processusGroup.h"
#define DIEU -1

extern Machine *machine;	// user program memory and registers
extern SynchConsole *synchconsole;

#define MAX_STRING_SIZE 30
#define MAX_INT_SIZE 10		//10 car le int max est sur 10 caracteres
#endif

#ifdef FILESYS_NEEDED		// FILESYS or FILESYS_STUB
#include "filesys.h"
extern FileSystem *fileSystem;
#endif

#ifdef FILESYS
#include "synchdisk.h"
extern SynchDisk *synchDisk;
#endif

/*
#ifdef NETWORK
#include "post.h"
extern PostOffice *postOffice;
#endif
*/

#endif // SYSTEM_H
