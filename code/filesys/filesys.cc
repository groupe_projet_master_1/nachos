// filesys.cc 
//	Routines to manage the overall operation of the file system.
//	Implements routines to map from textual file names to files.
//
//	Each file in the file system has:
//	   A file header, stored in a sector on disk 
//		(the size of the file header data structure is arranged
//		to be precisely the size of 1 disk sector)
//	   A number of data blocks
//	   An entry in the file system directory
//
// 	The file system consists of several data structures:
//	   A bitmap of free disk sectors (cf. bitmap.h)
//	   A directory of file names and file headers
//
//      Both the bitmap and the directory are represented as normal
//	files.  Their file headers are located in specific sectors
//	(sector 0 and sector 1), so that the file system can find them 
//	on bootup.
//
//	The file system assumes that the bitmap and directory files are
//	kept "open" continuously while Nachos is running.
//
//	For those operations (such as Create, Remove) that modify the
//	directory and/or bitmap, if the operation succeeds, the changes
//	are written immediately back to disk (the two files are kept
//	open during all this time).  If the operation fails, and we have
//	modified part of the directory and/or bitmap, we simply discard
//	the changed version, without writing it back to disk.
//
// 	Our implementation at this point has the following restrictions:
//
//	   there is no synchronization for concurrent accesses
//	   files have a fixed size, set when the file is created
//	   files cannot be bigger than about 3KB in size
//	   there is no hierarchical directory structure, and only a limited
//	     number of files can be added to the system
//	   there is no attempt to make the system robust to failures
//	    (if Nachos exits in the middle of an operation that modifies
//	    the file system, it may corrupt the disk)
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"

#include "disk.h"
#include "bitmap.h"
#include "directory.h"
#include "filehdr.h"
#include "filesys.h"
#include "system.h"

// Initial file sizes for the bitmap and directory; until the file system
// supports extensible files, the directory size sets the maximum number 
// of files that can be loaded onto the disk.
#define FreeMapFileSize 	(NumSectors / BitsInByte)
#define DirectoryFileSize 	(sizeof(DirectoryEntry) * NumDirEntries)

//----------------------------------------------------------------------
// FileSystem::FileSystem
// 	Initialize the file system.  If format = TRUE, the disk has
//	nothing on it, and we need to initialize the disk to contain
//	an empty directory, and a bitmap of free sectors (with almost but
//	not all of the sectors marked as free).  
//
//	If format = FALSE, we just have to open the files
//	representing the bitmap and the directory.
//
//	"format" -- should we initialize the disk?
//----------------------------------------------------------------------

FileSystem::FileSystem(bool format)
{ 
    DEBUG('f', "Initializing the file system.\n");
    if (format) {
        BitMap *freeMap = new BitMap(NumSectors);
        Directory *directory = new Directory(NumDirEntries);
	FileHeader *mapHdr = new FileHeader;
	FileHeader *dirHdr = new FileHeader;

        DEBUG('f', "Formatting the file system.\n");

    // First, allocate space for FileHeaders for the directory and bitmap
    // (make sure no one else grabs these!)
	freeMap->Mark(FreeMapSector);	    
	freeMap->Mark(DirectorySector);

    // Second, allocate space for the data blocks containing the contents
    // of the directory and bitmap files.  There better be enough space!

	ASSERT(mapHdr->Allocate(freeMap, FreeMapFileSize));
	ASSERT(dirHdr->Allocate(freeMap, DirectoryFileSize));

    // Flush the bitmap and directory FileHeaders back to disk
    // We need to do this before we can "Open" the file, since open
    // reads the file header off of disk (and currently the disk has garbage
    // on it!).

        DEBUG('f', "Writing headers back to disk.\n");
	mapHdr->WriteBack(FreeMapSector);    
	dirHdr->WriteBack(DirectorySector);

    // OK to open the bitmap and directory files now
    // The file system operations assume these two files are left open
    // while Nachos is running.

        freeMapFile = new OpenFile(FreeMapSector);
        SetWorkingDirectory(new OpenFile(DirectorySector));
        fileCollection = new FileCollection();

        directory->AddSelfDirectory(GetWorkingDirectory()->GetSector());

    // Once we have the files "open", we can write the initial version
    // of each file back to disk.  The directory at this point is completely
    // empty; but the bitmap has been changed to reflect the fact that
    // sectors on the disk have been allocated for the file headers and
    // to hold the file data for the directory and bitmap.

        DEBUG('f', "Writing bitmap and directory back to disk.\n");
        freeMap->WriteBack(freeMapFile);	 // flush changes to disk
        directory->WriteBack(GetWorkingDirectory());

        if (DebugIsEnabled('f')) {
            freeMap->Print();
            directory->Print();
        }

        delete freeMap; 
        delete directory; 
        delete mapHdr; 
        delete dirHdr;
    } else {
    // if we are not formatting the disk, just open the files representing
    // the bitmap and directory; these are left open while Nachos is running
        freeMapFile = new OpenFile(FreeMapSector);
        SetWorkingDirectory(new OpenFile(DirectorySector));
        fileCollection = new FileCollection();
    }
}

//----------------------------------------------------------------------
// FileSystem::SaveWorkingDirectory
//  Save the working directory to restore it later.
//----------------------------------------------------------------------

void
FileSystem::SaveWorkingDirectory()
{
    DEBUG('f', "Saving current working directory\n");
    SetSavedWorkingDirectory(GetWorkingDirectory());
}

//----------------------------------------------------------------------
// FileSystem::RestoreWorkingDirectory
//  Restore the previously saved working directory.
//----------------------------------------------------------------------

void
FileSystem::RestoreWorkingDirectory()
{
    if (GetSavedWorkingDirectory() != NULL) {
        DEBUG('f', "Switching back to saved working directory\n");
        if (GetWorkingDirectory() != GetSavedWorkingDirectory()) {
            delete GetWorkingDirectory();
        }
        SetWorkingDirectory(GetSavedWorkingDirectory());
        SetSavedWorkingDirectory(NULL);
    }
}

//----------------------------------------------------------------------
// FileSystem::SeparateFileFromPath
//  Separate last string at end of path. The last string after a slash 
//	character '/' being supposedly the name of a file.
//	Given pointers will be updated as follows:
//		path -- given path minus string after last slash character
//		file -- string after last slash character
//
//  Example:
//		// Before:	path = "a/b/c/d/test"  file = "a/b/c/d/test"
//      SeparateFileFromPath(path, file);
//		// After:	path = "a/b/c/d/"      file = "test"
//
//	path -- pointer to path of directories bringing to file
//	file -- pointer to resulting file name
//----------------------------------------------------------------------

void
FileSystem::SeparateFileFromPath(char *path, char *file)
{
    char *next, *tmppath;

    next = strdup(path);
    tmppath = strdup(path);
    do {
        strcpy(file, next);
        next = strsep(&tmppath, "/");
    } while (next != NULL);
    
    for (int i = strlen(path); i >= 0 && path[i] != '/'; i--) {
        path[i] = '\0';
    }

    delete tmppath;
    delete next;
}

//----------------------------------------------------------------------
// FileSystem::Create
// 	Create a file in the Nachos file system (similar to UNIX create).
//	Since we can't increase the size of files dynamically, we have
//	to give Create the initial size of the file.
//
//	The steps to create a file are:
//	  Make sure the file doesn't already exist
//        Allocate a sector for the file header
// 	  Allocate space on disk for the data blocks for the file
//	  Add the name to the directory
//	  Store the new file header on disk 
//	  Flush the changes to the bitmap and the directory back to disk
//
//	Return TRUE if everything goes ok, otherwise, return FALSE.
//
// 	Create fails if:
//   		file is already in directory
//	 	no free space for file header
//	 	no free entry for file in directory
//	 	no free space for data blocks for the file 
//
// 	Note that this implementation assumes there is no concurrent access
//	to the file system!
//
//	"name" -- name of file to be created
//	"initialSize" -- size of file to be created
//  "isDirectory" -- if created file is a directory, set to true
//----------------------------------------------------------------------

bool
FileSystem::Create(const char *name, int initialSize)
{
    return Create(name, initialSize, false);
}

bool
FileSystem::CreateDirectory(const char *name)
{
    return Create(name, ((FileNameMaxLen + 1) + sizeof(FileHeader)) * NumDirEntries, true);
}

bool
FileSystem::Create(const char *name, int initialSize, bool isDirectory)
{
    Directory *directory;
    BitMap *freeMap;
    FileHeader *hdr;
    int sector;
    bool success;
    char *toCreate, *path;

    path = strdup(name);
    toCreate = strdup(name);
    SeparateFileFromPath(path, toCreate);

    SaveWorkingDirectory();
    ChangeDirectory(path);

    DEBUG('f', "Creating file %s, size %d\n", toCreate, initialSize);

    directory = new Directory(NumDirEntries);
    directory->FetchFrom(GetWorkingDirectory());

    if (toCreate[0] == '\0') {
        success = FALSE;
    }
    else if (directory->Find(toCreate) != -1)
      success = FALSE;			// file is already in directory
    else {
        freeMap = new BitMap(NumSectors);
        freeMap->FetchFrom(freeMapFile);
        sector = freeMap->Find();	// find a sector to hold the file header
    	if (sector == -1) 		
            success = FALSE;		// no free block for file header
	    else {
    	    hdr = new FileHeader;
            if (!hdr->Allocate(freeMap, initialSize))
                success = FALSE;	// no space on disk for data
            else {
                hdr->WriteBack(sector);
                if (!directory->Add(toCreate, sector, isDirectory))
                    success = FALSE;	// no space in directory
                else {
                    success = TRUE;
                    // everthing worked, flush all changes back to disk
                    directory->WriteBack(GetWorkingDirectory());
                    freeMap->WriteBack(freeMapFile);
                }
            }
            delete hdr;
	    }
        delete freeMap;
    }
    delete directory;
    delete toCreate;
    delete path;

    RestoreWorkingDirectory();
    
    return success;
}

//----------------------------------------------------------------------
// FileSystem::Open
// 	Open a file for reading and writing and add it to the opened files
//  collection. Return the index at which the file  has been added in 
//  the collection.
//	To open a file:
//	  Find the location of the file's header, using the directory 
//	  Bring the header into memory
//    Add the opened file to the file collection
//
//	"name" -- the text name of the file to be opened
//----------------------------------------------------------------------

int
FileSystem::Open(const char *name)
{ 
    Directory *directory = new Directory(NumDirEntries);
    OpenFile *openFile = NULL;
    int sector, fid = -1;

    DEBUG('f', "Opening file %s\n", name);
    directory->FetchFrom(GetWorkingDirectory());
    sector = directory->Find(name);
    if (sector >= 0 && (! directory->IsDirectory(name))) {
        openFile = new OpenFile(sector);   // name was found in directory
        fid = fileCollection->Add(openFile);
        if (fid == -1) {
            delete openFile;    // no space in the file collection
        }
    }
    delete directory;
    return fid;	// return -1 if not found
}

//----------------------------------------------------------------------
// FileSystem::Close
//  Close a file and remove it from the file collection at the 
//  specified index. If no file was opened at the specified index, 
//  return 0. Otherwise, if successful, return 1.
//
//  fid -- index of the opened file to remove
//----------------------------------------------------------------------

int
FileSystem::Close(int fid)
{
    return fileCollection->Remove(fid);
}

//----------------------------------------------------------------------
// FileSystem::Read
//  Read from an opened file added to the file collection. Return the 
//  number of bytes read.
//
//  fid -- index of the opened file
//  into -- the buffer to contain the data to be read from disk
//  numBytes -- the number of bytes to transfer
//----------------------------------------------------------------------

int
FileSystem::Read(const int fid, char *into, int numBytes)
{
    return fileCollection->Read(fid, into, numBytes);
}

//----------------------------------------------------------------------
// FileSystem::Write
//  Write to an opened file added to the file collection. Return the 
//  number of bytes written.
//
//  fid -- index of the opened file
//  from -- the buffer containing the data to be written to disk
//  numBytes -- the number of bytes to transfer
//----------------------------------------------------------------------

int
FileSystem::Write(const int fid, const char *from, int numBytes)
{
    return fileCollection->Write(fid, from, numBytes);
}

//----------------------------------------------------------------------
// FileSystem::GetOpenFile
//  Return the instance of an opened file added to the file collection 
//  at specified index.
//
//  fid -- index of the opened file to retrieve
//----------------------------------------------------------------------

OpenFile*
FileSystem::GetOpenFile(const int fid) {
    return fileCollection->GetOpenFile(fid);
}

//----------------------------------------------------------------------
// FileSystem::Remove
// 	Delete a file from the file system.  This requires:
//	    Remove it from the directory
//	    Delete the space for its header
//	    Delete the space for its data blocks
//	    Write changes to directory, bitmap back to disk
//
//  To delete a directory, this one needs to be empty.
//
//	Return TRUE if the file was deleted, FALSE if the file wasn't
//	in the file system.
//
//	"name" -- the text name of the file to be removed
//----------------------------------------------------------------------

bool
FileSystem::Remove(const char *name)
{
    return Remove(name, false);
}

bool
FileSystem::RemoveDirectory(const char *name)
{
    return Remove(name, true);
}

bool
FileSystem::Remove(const char *name, bool isDirectory)
{ 
    Directory *directory, *directoryToRemove;
    BitMap *freeMap;
    FileHeader *fileHdr;
    int sector;
    char *toRemove, *path;

    path = strdup(name);
    toRemove = strdup(name);
    SeparateFileFromPath(path, toRemove);

    SaveWorkingDirectory();
    ChangeDirectory(path);
    
    directory = new Directory(NumDirEntries);
    directory->FetchFrom(GetWorkingDirectory());
    sector = directory->Find(toRemove);
    if (sector == -1) {
       delete directory;
       return FALSE;			 // file not found 
    }
    if ((directory->IsDirectory(toRemove) && ! isDirectory)
    ||  (! directory->IsDirectory(toRemove) && isDirectory)) {
        delete directory;
        return FALSE;           // file is actually a directory, or
    }                           // directory is actually a file
    if (isDirectory) {
        RestoreWorkingDirectory();
        SaveWorkingDirectory();
        ChangeDirectory(name);
        directoryToRemove = new Directory(NumDirEntries);
        directoryToRemove->FetchFrom(GetWorkingDirectory());
        if (! directoryToRemove->IsEmpty()) {
            DEBUG('f', "Directory to remove not empty\n");
            RestoreWorkingDirectory();
            delete directoryToRemove;
            delete directory;
            return FALSE;           // directory to remove not empty
        }
        delete directoryToRemove;
        RestoreWorkingDirectory();
        SaveWorkingDirectory();
        ChangeDirectory(path);
    }
    fileHdr = new FileHeader;
    fileHdr->FetchFrom(sector);

    freeMap = new BitMap(NumSectors);
    freeMap->FetchFrom(freeMapFile);

    fileHdr->Deallocate(freeMap);  		// remove data blocks
    freeMap->Clear(sector);			// remove header block
    directory->Remove(toRemove);

    freeMap->WriteBack(freeMapFile);		// flush to disk
    directory->WriteBack(GetWorkingDirectory());        // flush to disk

    RestoreWorkingDirectory();

    delete fileHdr;
    delete directory;
    delete freeMap;
    return TRUE;
} 

//----------------------------------------------------------------------
// FileSystem::List
// 	List all the files in the file system directory.
//----------------------------------------------------------------------

void
FileSystem::List()
{
    Directory *directory = new Directory(NumDirEntries);

    directory->FetchFrom(GetWorkingDirectory());
    directory->List();
    delete directory;
}

//----------------------------------------------------------------------
// FileSystem::ChangeDirectory
//  Change the working directory to be path.
//
//  Return TRUE if succesful, FALSE otherwise.
//
//  "path" -- an absolute or relative pathname
//----------------------------------------------------------------------
bool
FileSystem::ChangeDirectory(const char *path)
{
    bool result = true;
    char *first, *next;
    int fid;

    if (path[0] != '\0') {
        DEBUG('f', "Changing working directory to %s\n", path);
        next = strdup(path);

        Directory *directory = new Directory(NumDirEntries);
        OpenFile *tmp = GetWorkingDirectory();

        // If absolute path
        if (next[0] == '/') {
            DEBUG('f', "Starting from root directory\n");
            tmp = new OpenFile(DirectorySector);
            directory->FetchFrom(tmp);
            first = strsep(&next, "/");
        }

        while ((first = strsep(&next, "/")) != NULL && first[0] != '\0') {
            DEBUG('f', "Looking for %s...\n", first);
            directory->FetchFrom(tmp);
            fid = directory->Find(first);
            if (fid != -1) {
                DEBUG('f', "Directory %s found\n", first);
                tmp = new OpenFile(fid);
                result = true;
            }
            else {
                DEBUG('f', "Directory %s does not exist\n", first);
                result = false;
                break;
            }
        }

        if (result) {
            SetWorkingDirectory(tmp);
        }
        else {
            delete tmp;
        }

        delete directory;
        delete next;
    }

    return result;
}

//----------------------------------------------------------------------
// FileSystem::PWD
//  Print name of current working directory.
//----------------------------------------------------------------------

void
FileSystem::PWD()
{
    int i = 1;
    char *pwd, *pwdtmp, tmp[FileNameMaxLen + 1];
    Directory *directory;
    pwd = (char *) malloc(sizeof(char));
    strcpy(pwd, "");
    SaveWorkingDirectory();
    while (GetWorkingDirectory()->GetSector() != DirectorySector) {
        i++;
        directory = new Directory(NumDirEntries);
        directory->FetchFrom(GetWorkingDirectory());
        strcpy(tmp, directory->GetName());
        pwdtmp = (char *) malloc((FileNameMaxLen + 1) * (i - 1) * sizeof(char));
        strcpy(pwdtmp, pwd);
        delete pwd;
        pwd = (char *) malloc((FileNameMaxLen + 1) * i * sizeof(char));
        strcpy(pwd, "/");
        strcat(pwd, tmp);
        strcat(pwd, pwdtmp);
        delete pwdtmp;
        delete directory;
        ChangeDirectory("..");
    }
    RestoreWorkingDirectory();
    printf("%s", pwd);
    fflush(stdout);
    delete pwd;
}

//----------------------------------------------------------------------
// FileSystem::Print
// 	Print everything about the file system:
//	  the contents of the bitmap
//	  the contents of the directory
//	  for each file in the directory,
//	      the contents of the file header
//	      the data in the file
//----------------------------------------------------------------------

void
FileSystem::Print()
{
    FileHeader *bitHdr = new FileHeader;
    FileHeader *dirHdr = new FileHeader;
    BitMap *freeMap = new BitMap(NumSectors);
    Directory *directory = new Directory(NumDirEntries);

    printf("Bit map file header:\n");
    bitHdr->FetchFrom(FreeMapSector);
    bitHdr->Print();

    printf("Directory file header:\n");
    dirHdr->FetchFrom(DirectorySector);
    dirHdr->Print();

    freeMap->FetchFrom(freeMapFile);
    freeMap->Print();

    directory->FetchFrom(GetWorkingDirectory());
    directory->Print();

    delete bitHdr;
    delete dirHdr;
    delete freeMap;
    delete directory;
}

OpenFile*
FileSystem::GetWorkingDirectory() {
    return currentThread->directoryFile;
}

OpenFile*
FileSystem::GetSavedWorkingDirectory() {
    return currentThread->savedDirectoryFile;
}

void
FileSystem::SetWorkingDirectory(OpenFile *wd) {
    currentThread->directoryFile = wd;
}

void
FileSystem::SetSavedWorkingDirectory(OpenFile *wd) {
    currentThread->savedDirectoryFile = wd;
}
