// filesys.h 
//	Data structures to represent the Nachos file system.
//
//	A file system is a set of files stored on disk, organized
//	into directories.  Operations on the file system have to
//	do with "naming" -- creating, opening, and deleting files,
//	given a textual file name.  Operations on an individual
//	"open" file (read, write, close) are to be found in the OpenFile
//	class (openfile.h).
//
//	We define two separate implementations of the file system. 
//	The "STUB" version just re-defines the Nachos file system 
//	operations as operations on the native UNIX file system on the machine
//	running the Nachos simulation.  This is provided in case the
//	multiprogramming and virtual memory assignments (which make use
//	of the file system) are done before the file system assignment.
//
//	The other version is a "real" file system, built on top of 
//	a disk simulator.  The disk is simulated using the native UNIX 
//	file system (in a file named "DISK"). 
//
//	In the "real" implementation, there are two key data structures used 
//	in the file system.  There is a single "root" directory, listing
//	all of the files in the file system; unlike UNIX, the baseline
//	system does not provide a hierarchical directory structure.  
//	In addition, there is a bitmap for allocating
//	disk sectors.  Both the root directory and the bitmap are themselves
//	stored as files in the Nachos file system -- this causes an interesting
//	bootstrap problem when the simulated disk is initialized. 
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#ifndef FS_H
#define FS_H

#include "copyright.h"
#include "filecollection.h"
#include "openfile.h"

// Sectors containing the file headers for the bitmap of free sectors,
// and the directory of files.  These file headers are placed in well-known 
// sectors, so that they can be located on boot-up.
#define FreeMapSector 		0
#define DirectorySector 	1

#ifdef FILESYS_STUB 		// Temporarily implement file system calls as 
				// calls to UNIX, until the real file system
				// implementation is available
class FileSystem {
  public:
    FileSystem(bool format) {}

    bool Create(const char *name, int initialSize) { 
	int fileDescriptor = OpenForWrite(name);

	if (fileDescriptor == -1) return FALSE;
	Close(fileDescriptor); 
	return TRUE; 
	}

    OpenFile* Open(char *name) {
	  int fileDescriptor = OpenForReadWrite(name, FALSE);

	  if (fileDescriptor == -1) return NULL;
	  return new OpenFile(fileDescriptor);
      }

    bool Remove(char *name) { return Unlink(name) == 0; }

};

#else // FILESYS
class FileSystem {
  public:
    FileSystem(bool format);		// Initialize the file system.
					// Must be called *after* "synchDisk" 
					// has been initialized.
    					// If "format", there is nothing on
					// the disk, so initialize the directory
    					// and the bitmap of free blocks.

    bool Create(const char *name, int initialSize);
    bool CreateDirectory(const char *name);

    int Open(const char *name); 	// Open a file (UNIX open)

	int Close(int fid);

	int Read(const int fid, char *into, int numBytes);

	int Write(const int fid, const char *from, int numBytes);

	OpenFile *GetOpenFile(const int fid);

    bool Remove(const char *name);		// Delete a file
    bool RemoveDirectory(const char *name);	// Delete a directory

    void List();			// List all the files in the file system

	bool ChangeDirectory(const char *path);	// Change the working directory

    void PWD();			// Print name of current working directory

    void Print();			// List all the files and their contents

  private:
	OpenFile* freeMapFile;		// Bit map of free disk blocks,
					// represented as a file

	FileCollection* fileCollection;		// Collection of opened files

	void SaveWorkingDirectory();	// Save the working directory to
					// restore it later
	void RestoreWorkingDirectory();	// Restore the previously saved 
					// working directory

	void SeparateFileFromPath(char *path, char *file);

    bool Create(const char *name, int initialSize, bool isDirectory);  	
					// Create a file (UNIX creat)
    bool Remove(const char *name, bool isDirectory);
					// Delete a file (UNIX unlink)
	OpenFile *GetWorkingDirectory();
	OpenFile *GetSavedWorkingDirectory();

	void SetWorkingDirectory(OpenFile *wd);
	void SetSavedWorkingDirectory(OpenFile *wd);
};

#endif // FILESYS

#endif // FS_H
