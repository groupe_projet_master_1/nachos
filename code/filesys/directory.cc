// directory.cc 
//	Routines to manage a directory of file names.
//
//	The directory is a table of fixed length entries; each
//	entry represents a single file, and contains the file name,
//	and the location of the file header on disk.  The fixed size
//	of each directory entry means that we have the restriction
//	of a fixed maximum size for file names.
//
//	The constructor initializes an empty directory of a certain size;
//	we use ReadFrom/WriteBack to fetch the contents of the directory
//	from disk, and to write back any modifications back to disk.
//
//	Also, this implementation has the restriction that the size
//	of the directory cannot expand.  In other words, once all the
//	entries in the directory are used, no more files can be created.
//	Fixing this is one of the parts to the assignment.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "utility.h"
#include "filehdr.h"
#include "directory.h"

//----------------------------------------------------------------------
// Directory::Directory
// 	Initialize a directory; initially, the directory is completely
//	empty.  If the disk is being formatted, an empty directory
//	is all we need, but otherwise, we need to call FetchFrom in order
//	to initialize it from disk.
//
//	"size" is the number of entries in the directory
//----------------------------------------------------------------------

Directory::Directory(int size)
{
    table = new DirectoryEntry[size];
    tableSize = size;
    for (int i = 0; i < tableSize; i++)
	table[i].inUse = FALSE;
}

//----------------------------------------------------------------------
// Directory::~Directory
// 	De-allocate directory data structure.
//----------------------------------------------------------------------

Directory::~Directory()
{ 
    delete [] table;
} 

//----------------------------------------------------------------------
// Directory::FetchFrom
// 	Read the contents of the directory from disk.
//
//	"file" -- file containing the directory contents
//----------------------------------------------------------------------

void
Directory::FetchFrom(OpenFile *file)
{
    sector = file->GetSector();
    (void) file->ReadAt(directoryName, FileNameMaxLen + 1, 0);
    (void) file->ReadAt((char *)table, tableSize * sizeof(DirectoryEntry), FileNameMaxLen + 1);
}

//----------------------------------------------------------------------
// Directory::WriteBack
// 	Write any modifications to the directory back to disk
//
//	"file" -- file to contain the new directory contents
//----------------------------------------------------------------------

void
Directory::WriteBack(OpenFile *file)
{
    sector = file->GetSector();
    (void) file->WriteAt(directoryName, FileNameMaxLen + 1, 0);
    (void) file->WriteAt((char *)table, tableSize * sizeof(DirectoryEntry), FileNameMaxLen + 1);
}

//----------------------------------------------------------------------
// Directory::FindIndex
// 	Look up file name in directory, and return its location in the table of
//	directory entries.  Return -1 if the name isn't in the directory.
//
//	"name" -- the file name to look up
//----------------------------------------------------------------------

int
Directory::FindIndex(const char *name)
{
    for (int i = 0; i < tableSize; i++)
        if (table[i].inUse && !strncmp(table[i].name, name, FileNameMaxLen))
	    return i;
    return -1;		// name not in directory
}

//----------------------------------------------------------------------
// Directory::Find
// 	Look up file name in directory, and return the disk sector number
//	where the file's header is stored. Return -1 if the name isn't 
//	in the directory.
//
//	"name" -- the file name to look up
//----------------------------------------------------------------------

int
Directory::Find(const char *name)
{
    int i = FindIndex(name);

    if (i != -1)
	return table[i].sector;
    return -1;
}

//----------------------------------------------------------------------
// AddSelfDirectory
//  Add a directory which parent is its own directory.
//----------------------------------------------------------------------

void
Directory::AddSelfDirectory()
{
    AddSelfDirectory(sector);
}

void
Directory::AddSelfDirectory(int selfsector)
{
    table[0].inUse = TRUE;
    table[0].isDirectory = TRUE;
    table[0].sector = selfsector;
    strncpy(table[0].name, ".", FileNameMaxLen); 
}

//----------------------------------------------------------------------
// AddParentDirectory
//  Add a directory which parent is the previous directory.
//
//  "parentsector" -- the disk sector containing this directory
//----------------------------------------------------------------------

void
Directory::AddParentDirectory(int parentsector)
{
    table[1].inUse = TRUE;
    table[1].isDirectory = TRUE;
    table[1].sector = parentsector;
    strncpy(table[1].name, "..", FileNameMaxLen); 
}

//----------------------------------------------------------------------
// Directory::Add
// 	Add a file into the directory.  Return TRUE if successful;
//	return FALSE if the file name is already in the directory, or if
//	the directory is completely full, and has no more space for
//	additional file names.
//
//	"name" -- the name of the file being added
//	"newSector" -- the disk sector containing the added file's header
//  "isDirectory" -- if added file is a directory, set to true
//----------------------------------------------------------------------

bool
Directory::Add(const char *name, int newSector, bool isDirectory)
{
    if (FindIndex(name) != -1)
	    return FALSE;

    for (int i = 0; i < tableSize; i++)
        if (!table[i].inUse) {
            table[i].inUse = TRUE;
            strncpy(table[i].name, name, FileNameMaxLen);
            table[i].sector = newSector;
            table[i].isDirectory = isDirectory;
            if (isDirectory) {
                OpenFile *createdDirectoryFile = new OpenFile(newSector);
                Directory *createdDirectory = new Directory(NumDirEntries);
                createdDirectory->SetName(name);
                createdDirectory->AddSelfDirectory(newSector);
                createdDirectory->AddParentDirectory(sector);
                createdDirectory->WriteBack(createdDirectoryFile);
                delete createdDirectory;
                delete createdDirectoryFile;
            }
        return TRUE;
	}
    return FALSE;	// no space.  Fix when we have extensible files.
}

//----------------------------------------------------------------------
// Directory::Remove
// 	Remove a file name from the directory.  Return TRUE if successful;
//	return FALSE if the file isn't in the directory. 
//
//	"name" -- the file name to be removed
//----------------------------------------------------------------------

bool
Directory::Remove(const char *name)
{ 
    int i = FindIndex(name);

    if (i == -1)
	return FALSE; 		// name not in directory
    table[i].inUse = FALSE;
    return TRUE;	
}

//----------------------------------------------------------------------
// Directory::List
// 	List all the file names in the directory. 
//----------------------------------------------------------------------

void
Directory::List()
{
    FileHeader *hdr = new FileHeader;
    char directory;
    for (int i = 0; i < tableSize; i++) {
        if (table[i].inUse) {
            directory = '-';
            hdr->FetchFrom(table[i].sector);
            if (table[i].isDirectory) {
                directory = 'd';
            }
            printf("%c %6d %s\n", directory, hdr->FileLength(), table[i].name);
        }
    }
}

//----------------------------------------------------------------------
// Directory::Print
// 	List all the file names in the directory, their FileHeader locations,
//	and the contents of each file.  For debugging.
//----------------------------------------------------------------------

void
Directory::Print()
{ 
    FileHeader *hdr = new FileHeader;

    printf("Directory contents:\n");
    for (int i = 0; i < tableSize; i++)
	if (table[i].inUse) {
	    printf("Name: %s, Sector: %d\n", table[i].name, table[i].sector);
	    hdr->FetchFrom(table[i].sector);
	    hdr->Print();
	}
    printf("\n");
    delete hdr;
}

//----------------------------------------------------------------------
// Directory::GetName
//  Return directory's name.
//----------------------------------------------------------------------

const char*
Directory::GetName()
{
   return (const char *) directoryName;
}

//----------------------------------------------------------------------
// Directory::SetName
//  Set directory's name.
//----------------------------------------------------------------------

void
Directory::SetName(const char *name)
{
   strcpy(directoryName, name);
}

//----------------------------------------------------------------------
// Directory::IsEmpty
//  Return FALSE if a file or directory is detected, TRUE otherwise.
//----------------------------------------------------------------------

bool
Directory::IsEmpty()
{
    // Start at 2 to exclude "." and ".."
    for (int i = 2; i < tableSize; i++)
        if (table[i].inUse)
            return FALSE;
    return TRUE;
}

//----------------------------------------------------------------------
// Directory::IsDirectory
// 	Look up file name in directory, and return the boolean that was set
//	during the creation of the file. Return TRUE if the file name
//	resolves to be a directory, FALSE otherwise.
//
//	"name" -- the file name to look up
//----------------------------------------------------------------------

bool
Directory::IsDirectory(const char *name)
{
    int i = FindIndex(name);

    if (i != -1)
	    return table[i].isDirectory;
    return FALSE;
}
