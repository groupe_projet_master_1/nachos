#include "filecollection.h"

#include "bitmap.h"
#include "synch.h"
#include "system.h"

//----------------------------------------------------------------------
// FileCollection::FileCollection
//----------------------------------------------------------------------

FileCollection::FileCollection() {
    collection = new OpenFile*[MAX_OPEN_FILES];
    bitmap = new BitMap(MAX_OPEN_FILES);
    lock = new Lock("filecollection");
    fileLocks = new Lock*[MAX_OPEN_FILES];
}

//----------------------------------------------------------------------
// FileCollection::~FileCollection
//----------------------------------------------------------------------

FileCollection::~FileCollection() {
    delete [] fileLocks;
    delete lock;
    delete bitmap;
    delete [] collection;
}

//----------------------------------------------------------------------
// FileCollection::Add
//  Add a opened file to the file collection and return the index at 
//  which the file has been added in the collection.
//
//  openFile -- opened file to add
//----------------------------------------------------------------------

int FileCollection::Add(OpenFile *openFile) {
    lock->Acquire();
    int fid = bitmap->Find();
    if (fid >= 0 && Find(openFile) == -1) {
        collection[fid] = openFile;
        fileLocks[fid] = new Lock("openFile");
        bitmap->Mark(fid);
    }
    lock->Release();
    return fid;
}

//----------------------------------------------------------------------
// FileCollection::Remove
//  Remove a file from the file collection at the specified index.
//  Return 0 if fid isn't the id of an opened file, 1 if successful.
//
//  fid -- index of the opened file to remove
//----------------------------------------------------------------------

int FileCollection::Remove(const int fid) {
    int result = 0;
    lock->Acquire();
    if (bitmap->Test(fid)) {
        fileLocks[fid]->Acquire();
        delete collection[fid];
        collection[fid] = NULL;
        bitmap->Clear(fid);
        delete fileLocks[fid];
        fileLocks[fid] = NULL;
        result = 1;
    }
    lock->Release();
    return result;
}

//----------------------------------------------------------------------
// FileCollection::Read
//  Read from an opened file added to the file collection. Return -1 if 
//  no opened file found with fid. Otherwise, return the number of 
//  bytes read.
//
//  fid -- index of the opened file
//  into -- the buffer to contain the data to be read from disk
//  numBytes -- the number of bytes to transfer
//----------------------------------------------------------------------

int FileCollection::Read(const int fid, char *into, int numBytes) {
    int nbRead = -1;
    fileLocks[fid]->Acquire();
    if (bitmap->Test(fid)) {
        nbRead = collection[fid]->Read(into, numBytes);
    }
    fileLocks[fid]->Release();
    return nbRead;
}

//----------------------------------------------------------------------
// FileCollection::Write
//  Write to an opened file added to the file collection. Return -1 if 
//  no opened file found with fid. Otherwise, return the number of 
//  bytes written.
//
//  fid -- index of the opened file
//  from -- the buffer containing the data to be written to disk
//  numBytes -- the number of bytes to transfer
//----------------------------------------------------------------------

int FileCollection::Write(const int fid, const char *from, int numBytes) {
    int nbWrote = -1;
    fileLocks[fid]->Acquire();
    if (bitmap->Test(fid)) {
        nbWrote = collection[fid]->Write(from, numBytes);
    }
    fileLocks[fid]->Release();
    return nbWrote;
}

//----------------------------------------------------------------------
// FileCollection::Find
//  If file is opened and stored in the collection, return its index 
//  in the file collection. Otherwise, returns -1.
//
//  openFile -- file to look for
//----------------------------------------------------------------------

int FileCollection::Find(OpenFile *openFile) {
    for (int i = 0; i < MAX_OPEN_FILES; i++) {
        if (collection[i] != NULL && openFile->GetSector() == collection[i]->GetSector()) {
            return i;
        }
    }
    return -1;
}

//----------------------------------------------------------------------
// FileCollection::GetOpenFile
//  Return the instance of an opened file added to the file collection 
//  at specified index.
//
//  fid -- index of the opened file to retrieve
//----------------------------------------------------------------------

OpenFile *FileCollection::GetOpenFile(const int fid) {
    return collection[fid];
}
