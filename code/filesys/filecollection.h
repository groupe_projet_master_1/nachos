#ifndef __FILE_COLLECTION_H__
#define  __FILE_COLLECTION_H__

class OpenFile;
class BitMap;
class Lock;

#define MAX_OPEN_FILES 10

class FileCollection {
    public:
        FileCollection();
        ~FileCollection();

        int Add(OpenFile *openFile);    // Add a opened file to the file collection
        int Remove(const int fid);     // Remove a file from the file collection

        int Read(const int fid, char *into, int numBytes);  // Read from an opened file added
                                        // to the file collection
        int Write(const int fid, const char *from, int numBytes);   // Write to an opened file added
                                        // to the file collection

        OpenFile *GetOpenFile(const int fid);   // Returns the instance of an opened file
                                        // added to the file collection

    private:
        OpenFile **collection;          // Collection of opened files
        BitMap *bitmap;                 // Bit map of used and unused ids
        Lock *lock;                     // General collection lock
        Lock **fileLocks;               // List of locks for appropriate files in the collection (same indeces)

        int Find(OpenFile *openFile);   // Returns the index of a opened file in the collection
};

#endif // __FILE_COLLECTION_H__
